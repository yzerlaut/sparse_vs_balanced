import sys
from graphs.my_graph import *
from neural_network_dynamics.theory.fitting_tf import fit_data

### ------------------------------------------
### ----------------- PAPER VERSION  ----
### ------------------------------------------
ORDER = 2
if sys.argv[-1]=='run':
    data_exc = np.load('sparse_vs_balanced/data/tf_data_exc2.npy').item()
    data_exc['Model']['COEFFS'] = fit_data(data_exc, order=ORDER)
    data_inh = np.load('sparse_vs_balanced/data/tf_data_inh2.npy').item()
    data_inh['Model']['COEFFS'] = fit_data(data_inh, order=ORDER)

    # from neural_network_dynamics.theory.tf import make_tf_plot
    # fig = make_tf_plot(data_inh, ylim=[0.01,50],
    #                    col_subsmpl=np.concatenate([np.arange(5)*3]),
    #                    with_theory=True)
    # show()

    from model import Model; Model2 = Model.copy()
    from neural_network_dynamics.theory.mean_field import find_fp, show_phase_space, get_full_statistical_quantities

    Model2['COEFFS_RecExc'] = data_exc['Model']['COEFFS']
    Model2['COEFFS_RecInh'] = data_inh['Model']['COEFFS']
    np.savez(desktop+'coeffs.npz', exc=data_exc['Model']['COEFFS'], inh=data_inh['Model']['COEFFS'])
    
    # Faff = 5. # level of afferent input (Sparse Activity setting)
    # fig, SA_output = show_phase_space(Model2,
    #                                   with_trajectory=[0.01, 0.01], t=np.arange(5000)*1e-4, 
    #                                   KEY1='RecExc', KEY2='RecInh',
    #                                   F1 = np.logspace(-2., 2.1, 200),
    #                                   F2 = np.logspace(-2., 2.2, 500),
    #                                   KEY_RATES1 = ['AffExc'], VAL_RATES1=[Faff],
    #                                   KEY_RATES2 = ['AffExc'], VAL_RATES2=[Faff])

    FA = np.load('sparse_vs_balanced/data/varying_AffExc_analyzed.npy')[0]
    FAFF2 = np.logspace(np.log(FA[0])/np.log(10), np.log(FA[-1])/np.log(10), 100)
    FE, FI, IE_REC, IE_AFF, II_REC, IRATIO = [], [], [], [], [], []
    for faff in FAFF2:
        fe_fp, fi_fp = find_fp(Model2,
                               t = np.arange(5000)*1e-4, X0=[0.1, 0.1],
                               KEY1='RecExc', KEY2='RecInh',
                               KEY_RATES1 = ['AffExc'], VAL_RATES1=[faff],
                               KEY_RATES2 = ['AffExc'], VAL_RATES2=[faff])
        output = get_full_statistical_quantities(Model2, [fe_fp, fi_fp],
                               KEY1='RecExc', KEY2='RecInh',
                               KEY_RATES1 = ['AffExc'], VAL_RATES1=[faff],
                               KEY_RATES2 = ['AffExc'], VAL_RATES2=[faff])
        FE.append(output['F_RecExc'])
        FI.append(output['F_RecInh'])
        # on excitatory currents
        # IRATIO.append(np.abs(Isyn['RecInh']/(Isyn['RecExc']+Isyn['AffExc'])))
        Isyn = output['Isyn_RecExc']
        IE_REC.append(Isyn['RecExc'])
        II_REC.append(Isyn['RecInh'])
        IE_AFF.append(Isyn['AffExc'])
        IRATIO.append(np.abs(Isyn['RecInh']/(Isyn['RecExc']+Isyn['AffExc'])))

    FE = np.array(FE)    
    Fe_All = FE
    FI = np.array(FI)    
    Iratio = np.array(IRATIO)    
    FE[FE<0.01], FI[FI<0.01] = 0.011, 0.011 # same thing than for numerical sim. limiting the range of data vis.

    from sparse_vs_balanced.plot_state_signature import plot_act_vs_aff_level
    fig1, _ = plot_act_vs_aff_level('sparse_vs_balanced/data/varying_AffExc_analyzed.npy',
                                          Iratio_th=Iratio, Fe_th=FE, Fi_th=FI, Fa_th=FAFF2)
    fig1.suptitle('All factors')
    fig1.savefig(desktop+'1.svg')

    ### ------------------------------------------
    ### ----------------- ONLY MUV AND SIGMAV ----
    ### ------------------------------------------

    data_exc = np.load('sparse_vs_balanced/data/tf_data_exc2.npy').item()
    data_exc['Model']['COEFFS'] = fit_data(data_exc, order=ORDER,
                                           included_factors = ['muV', 'sV'])
    data_inh = np.load('sparse_vs_balanced/data/tf_data_inh2.npy').item()
    data_inh['Model']['COEFFS'] = fit_data(data_inh, order=ORDER,
                                           included_factors = ['muV', 'sV'])

    # from neural_network_dynamics.theory.tf import make_tf_plot
    # fig = make_tf_plot(data_inh, ylim=[0.01,50],
    #                    col_subsmpl=np.concatenate([np.arange(5)*3]),
    #                    with_theory=True)
    # show()

    from model import Model; Model2 = Model.copy()
    Model2['COEFFS_RecExc'] = data_exc['Model']['COEFFS']
    Model2['COEFFS_RecInh'] = data_inh['Model']['COEFFS']

    # Faff = 5. # level of afferent input (Sparse Activity setting)
    # fig, SA_output = show_phase_space(Model2,
    #                                   with_trajectory=[0.01, 0.01], t=np.arange(5000)*1e-4, 
    #                                   KEY1='RecExc', KEY2='RecInh',
    #                                   F1 = np.logspace(-2., 2.1, 200),
    #                                   F2 = np.logspace(-2., 2.2, 500),
    #                                   KEY_RATES1 = ['AffExc'], VAL_RATES1=[Faff],
    #                                   KEY_RATES2 = ['AffExc'], VAL_RATES2=[Faff])

    FA = np.load('sparse_vs_balanced/data/varying_AffExc_analyzed.npy')[0]
    FAFF2 = np.logspace(np.log(FA[0])/np.log(10), np.log(FA[-1])/np.log(10), 100)
    FE, FI, IE_REC, IE_AFF, II_REC, IRATIO = [], [], [], [], [], []
    for faff in FAFF2:
        fe_fp, fi_fp = find_fp(Model2,
                               t = np.arange(5000)*1e-4, X0=[0.1, 0.1],
                               KEY1='RecExc', KEY2='RecInh',
                               KEY_RATES1 = ['AffExc'], VAL_RATES1=[faff],
                               KEY_RATES2 = ['AffExc'], VAL_RATES2=[faff])
        output = get_full_statistical_quantities(Model2, [fe_fp, fi_fp],
                               KEY1='RecExc', KEY2='RecInh',
                               KEY_RATES1 = ['AffExc'], VAL_RATES1=[faff],
                               KEY_RATES2 = ['AffExc'], VAL_RATES2=[faff])
        FE.append(output['F_RecExc'])
        FI.append(output['F_RecInh'])
        # on excitatory currents
        # IRATIO.append(np.abs(Isyn['RecInh']/(Isyn['RecExc']+Isyn['AffExc'])))
        Isyn = output['Isyn_RecExc']
        IE_REC.append(Isyn['RecExc'])
        II_REC.append(Isyn['RecInh'])
        IE_AFF.append(Isyn['AffExc'])
        IRATIO.append(np.abs(Isyn['RecInh']/(Isyn['RecExc']+Isyn['AffExc'])))

    FE = np.array(FE)    
    Fe_muV_sV = FE
    FI = np.array(FI)    
    Iratio = np.array(IRATIO)    
    FE[FE<0.01], FI[FI<0.01] = 0.011, 0.011 # same thing than for numerical sim. limiting the range of data vis.

    fig1, _ = plot_act_vs_aff_level('sparse_vs_balanced/data/varying_AffExc_analyzed.npy',
                                          Iratio_th=Iratio, Fe_th=FE, Fi_th=FI, Fa_th=FAFF2)

    fig1.suptitle('only $\mu_V$ and $\sigma_V$')
    fig1.savefig(desktop+'2.svg')
    np.save('temp.npy', [np.array(FA), Fe_muV_sV, Fe_All])
else:
    Fa = np.load('sparse_vs_balanced/data/varying_AffExc_analyzed.npy')[0]
    Fe = np.load('sparse_vs_balanced/data/varying_AffExc_analyzed.npy')[4]
    _, Fe_muV_sV, Fe_All = np.load('temp.npy')
    fig, ax = plt.subplots(figsize=(2.8,2))
    # ax.plot(Fa[::3], np.abs(Fe_muV_sV-Fe[::3])/Fe[::3], label='only $\mu_V$ and $\sigma_V$')
    # ax.plot(Fa[::3], np.abs(Fe_All-Fe[::3])/Fe[::3], label='All')
    from data_analysis.processing.signanalysis import gaussian_smoothing
    ax.plot(Fa[1::3], gaussian_smoothing(np.abs(Fe_muV_sV-Fe[1::3]), 3), label='only $\mu_V$ and $\sigma_V$', lw=2)
    ax.plot(Fa[1::3], gaussian_smoothing(np.abs(Fe_All-Fe[1::3]), 3), label='All', lw=2)
    # ax.set_yscale('log')
    ax.set_xscale('log')
    ax.legend()
    set_plot(ax, ylabel='abs. error (Hz)', xlabel='$\nu_a$ (Hz)', xticks=[5, 10, 20], xticks_labels=['5', '10', '20'], num_yticks=3)
    ax2 = plt.axes([.2,.7,.15,.3])
    ax2.bar([0], np.sum(np.abs(Fe_muV_sV-Fe[1::3]))-60, bottom=60)
    ax2.bar([1], np.sum(np.abs(Fe_All-Fe[1::3]))-60, bottom=60)
    set_plot(ax2, ylabel='$\sum$ abs. error (Hz)', xticks=[], num_yticks=3)
    fig.savefig(desktop+'3.svg')
    show()
