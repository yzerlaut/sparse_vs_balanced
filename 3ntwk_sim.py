import numpy as np
from model import Model, pass_arguments_of_new_model
import os, sys

def my_logspace(x1, x2, n):
    return np.logspace(np.log(x1)/np.log(10), np.log(x2)/np.log(10), n)

def run_one_config_of_dep_on_ExcAff(Model_modif,
                                    filename='data.zip',
                                    Faff=my_logspace(3., 25, 20),
                                    seeds=np.arange(1, 4),
                                    dt=0.1, tstop=5000,\
                                    Sim_string ='',
                                    Analysis_string = '',
                                    Fetch_data_string = '',
                                    Fetch_analysis_string = ''):

    for s in [Sim_string, Analysis_string, Fetch_data_string, Fetch_analysis_string]:
        s += 'echo --------------------------------------------------------------------'
        s += 'echo -------'+filename+'------------------------------------'
    
    Model2 = Model.copy()
    Model2['dt'], Model2['tstop'] = dt, tstop
    for key, val in Model_modif.items():
        Model2[key] = val
    tf_file = 'sparse_vs_balanced/varying_AffExc.py'

    Sim_string += 'python '+tf_file+pass_arguments_of_new_model(Model2)
    Sim_string += ' --F_AffExc_array'
    for fa in Faff: Sim_string += ' '+str(fa)
    Sim_string += ' --SEEDS'
    for s in seeds: Sim_string += ' '+str(s)
    Sim_string += ' -df sparse_vs_balanced/data'+os.path.sep
    Sim_string += ' -f sparse_vs_balanced/data'+os.path.sep+filename+' --N_rec_Vm 10 & \n'

    # for the analysis, not possible to do it in parrallel for memory problems...
    Analysis_string += 'python '+tf_file+' -a -f sparse_vs_balanced/data'+os.path.sep+filename+' \n'

    Fetch_data_string += 'get_from_mirror_dir sparse_vs_balanced/data'+os.path.sep+filename+' \n'
    Fetch_analysis_string += 'get_from_mirror_dir sparse_vs_balanced/data/'+filename.replace('.zip', '_analyzed.npz')+' \n'

    return Sim_string, Analysis_string, Fetch_data_string, Fetch_analysis_string


Q_FACTOR = [0.1, 1., 5.] # chosen values to investigate

if sys.argv[-1]=='script':
    from sparse_vs_balanced.params_scan import write_bash_scripts
    FILENAMES = []
    Sim_string, Analysis_string, Fetch_data_string, Fetch_analysis_string = '', '', '', ''
    for q in Q_FACTOR:
        FILENAMES.append('ccnd_'+str(q)+'.zip')
        Sim_string, Analysis_string, Fetch_data_string, Fetch_analysis_string = \
            run_one_config_of_dep_on_ExcAff({'Q_RecExc_RecExc':Model['Q_RecExc_RecExc']*q,
                                             'Q_RecExc_RecInh':Model['Q_RecExc_RecInh']*q,\
                                             'Q_RecInh_RecExc':Model['Q_RecInh_RecExc']*q,
                                             'Q_RecInh_RecInh':Model['Q_RecInh_RecInh']*q},\
                                            filename=FILENAMES[-1],
                                            Sim_string = Sim_string,
                                            Analysis_string = Analysis_string,
                                            Fetch_data_string = Fetch_data_string,
                                            Fetch_analysis_string = Fetch_analysis_string)

    write_bash_scripts(Sim_string, Analysis_string,
                       Fetch_data_string, Fetch_analysis_string,
                       'sparse_vs_balanced/bash_scan/cell_correlate_ntwk_dyn.sh')
elif sys.argv[-1]=='plot':
    from graphs.my_graph import *
    FNS = ['sparse_vs_balanced/data/ccnd_0.1_analyzed.npz',
           'sparse_vs_balanced/data/ccnd_1.0_analyzed.npz',
           'sparse_vs_balanced/data/ccnd_5.0_analyzed.npz']
    TITLES = ['Weak', 'Moderate', 'Strong']
    fig, AX = figure(axes=(4,3))
    for a, ax in enumerate(AX[0]):
        output = dict(np.load(FNS[a]))
        Fa = output['FA']
        muV = output['MUV_EXC'].reshape(len(np.unique(Fa)),int(len(Fa)/len(np.unique(Fa)))).mean(axis=1)
        smuV = output['sMUV_EXC'].reshape(len(np.unique(Fa)),int(len(Fa)/len(np.unique(Fa)))).mean(axis=1)
        sV = output['SV_EXC'].reshape(len(np.unique(Fa)),int(len(Fa)/len(np.unique(Fa)))).mean(axis=1)
        ssV = output['sSV_EXC'].reshape(len(np.unique(Fa)),int(len(Fa)/len(np.unique(Fa)))).mean(axis=1)
        gV = output['GV_EXC'].reshape(len(np.unique(Fa)),int(len(Fa)/len(np.unique(Fa)))).mean(axis=1)
        sgV = output['sGV_EXC'].reshape(len(np.unique(Fa)),int(len(Fa)/len(np.unique(Fa)))).mean(axis=1)
        Tv = output['TV_EXC'].reshape(len(np.unique(Fa)),int(len(Fa)/len(np.unique(Fa)))).mean(axis=1)
        sTv = output['sTV_EXC'].reshape(len(np.unique(Fa)),int(len(Fa)/len(np.unique(Fa)))).mean(axis=1)
        Nue = output['FR_EXC'].reshape(len(np.unique(Fa)),int(len(Fa)/len(np.unique(Fa)))).mean(axis=1)
        sNue = output['sFR_EXC'].reshape(len(np.unique(Fa)),int(len(Fa)/len(np.unique(Fa)))).mean(axis=1)
        Nue[Nue<1e-2] = 1e-2
        sNue[(Nue-sNue)<1e-2] = Nue[(Nue-sNue)<1e-2]-1e-2
        snumin = Nue-sNue
        snumin[snumin<1e-2] = 1e-2
        AX[0][a].fill_between(muV, Nue+sNue, snumin, color=viridis(a/2.), lw=0, alpha=.5)
        AX[0][a].errorbar(muV, Nue, fmt='o', xerr=smuV, yerr=sNue, color=viridis(a/2.), lw=1, ms=2)
        AX[1][a].fill_between(muV, sV+ssV, sV-ssV, color=viridis(a/2.), lw=0, alpha=.5)
        AX[1][a].errorbar(muV, sV, fmt='o', xerr=smuV, yerr=ssV, color=viridis(a/2.), lw=1, ms=2)
        AX[2][a].fill_between(muV, gV+sgV, gV-sgV, color=viridis(a/2.), lw=0, alpha=.5)
        AX[2][a].errorbar(muV, gV, fmt='o', xerr=sgV, yerr=sgV, color=viridis(a/2.), lw=1, ms=2)
        AX[3][a].fill_between(muV, Tv+sTv, Tv-sTv, color=viridis(a/2.), lw=0, alpha=.5)
        AX[3][a].errorbar(muV, Tv, fmt='o', xerr=smuV, yerr=sTv, color=viridis(a/2.), lw=1, ms=2)
    for ax, ytl, yl, xlim in zip(AX[0], [['<0.01', '0.1', '1', '10'], [], []], ['$\\nu_e$ (Hz)', '', ''], [[-68,-55], [-67,-58], [-66, -54]]):
        set_plot(ax, yscale='log', xlim=xlim, xticks=[-65,-60], grid=True,
                 ylim=[0.0091, 40], yticks_labels=ytl, ylabel=yl)
    for ax, yl, xlim in zip(AX[1], ['$\sigma_V$ (mV)', '', ''], [[-68,-55],[-67,-58], [-66, -54]]):
        set_plot(ax, xlim=xlim, xticks=[-65,-60], grid=True, ylim=[2.6,5],ylabel=yl)
    for ax, yl, xlim in zip(AX[2], ['$\gamma_V$', '', ''], [[-68,-55],[-67,-58], [-66, -54]]):
        set_plot(ax, xlim=xlim, xticks=[-65,-60], grid=True, ylim=[-1.5,1.5],ylabel=yl)
    for ax, yl, xlim in zip(AX[3], ['$\\tau_V$ (ms)', '', ''], [[-68,-55],[-67,-58], [-66, -54]]):
        set_plot(ax, xlim=xlim, xticks=[-65,-60], grid=True, ylim=[0,25], xlabel='$\mu_V$ (mV)',ylabel=yl)
    fig.savefig(desktop+'fig.svg')
    show()
else:
    from graphs.my_graph import *
    FNS = ['sparse_vs_balanced/data/ccnd_1.0_analyzed.npz',
           'sparse_vs_balanced/data/ccnd_5.0_analyzed.npz']
    fig2, AX2 = figure(axes=(1,2), wspace=.5, figsize=(.78,.9))
    for a in range(len(FNS)):
        output = dict(np.load(FNS[a]))
        print(output.keys())
        Fa = output['FA']
        muV = output['MUV_EXC'].reshape(len(np.unique(Fa)),int(len(Fa)/len(np.unique(Fa)))).mean(axis=1)
        smuV = output['sMUV_EXC'].reshape(len(np.unique(Fa)),int(len(Fa)/len(np.unique(Fa)))).mean(axis=1)
        Nue = 1e3*output['FR_EXC'].reshape(len(np.unique(Fa)),int(len(Fa)/len(np.unique(Fa)))).mean(axis=1)
        sNue = 1e3*output['sFR_EXC'].reshape(len(np.unique(Fa)),int(len(Fa)/len(np.unique(Fa)))).mean(axis=1)
        Nue[Nue<1e-2] = 1e-2
        sNue[(Nue-sNue)<1e-2] = Nue[(Nue-sNue)<1e-2]-1e-2
        snumin = Nue-sNue
        snumin[snumin<1e-2] = 1e-2
        AX2[a].fill_between(muV, Nue+sNue, snumin, color=viridis(.5+a/2.), lw=0, alpha=.5)
        AX2[a].errorbar(muV, Nue, fmt='o', xerr=smuV, yerr=sNue, color=viridis(.5+a/2.), lw=1, ms=1.5)
        AX2[a-1].errorbar(muV, Nue, fmt='o', xerr=smuV, yerr=sNue, color='w', lw=1, ms=2, alpha=0.001)
    set_plot(AX2[0], yscale='log', xlim=[-67,-58], xticks=[-65,-60], grid=False,
             xlabel='$\mu_V$ (mV)', ylim=[0.0091, 40], yticks_labels=['<0.01', '0.1', '1', '10'])
    set_plot(AX2[1], yscale='log', ylim=[0.0091, 40], yticks=[0.01, 0.1, 1., 10], 
             xlim=[-67,-58], xticks=[-65,-60], grid=False, yticks_labels=[], xlabel='$\mu_V$ (mV)')
    AX2[0].annotate('$\\nu_e$ (Hz)', (-.57, .7), xycoords='axes fraction', rotation=90)
    # fig2.suptitle('cellular correlate of \n$\\nu_a$-modulated recurrent dynamics', fontsize=FONTSIZE)
    fig2.savefig(desktop+'fig2.svg')
    show()

    
        
