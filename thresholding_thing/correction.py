import numpy as np
import sys
sys.path.append('../')
from graphs.my_graph import *
#from OU import OU_with_threshold, plot1
import pandas
# For statistics. Requires statsmodels 5.0 or more
from statsmodels.formula.api import ols

# MODEL DATA
data = dict(np.load('mu_sigma_data.npz'))
DS = (data['Rec_Sigma']-data['Sigma'])/data['Rec_Sigma']

cond = (data['Sigma']<.15)
data0 = pandas.DataFrame({'x': data['Rec_Mean'][cond],
                          'y': data['Rec_Sigma'][cond],
                          'z': data['Fout'][cond],
                          'u': data['TauV'][cond],
                          'output': DS[cond]})
# Fit the model
model = ols("output ~ x + y + z + u", data0).fit()
print(model.summary())

## # EXP DATA
exp_data = dict(np.load('exp_data.npz'))
fr = dict(np.load('firing_data.npz'))['firing'][1:]
MIN_VM, MAX_VM = -81, -30 # 30mV between
norm_exp_muV, norm_exp_sV = (exp_data['muV']-MIN_VM)/(MAX_VM-MIN_VM), exp_data['sV']/(MAX_VM-MIN_VM)
dataExp=pandas.DataFrame({'x': norm_exp_muV, 
                          'y': norm_exp_sV,
                          'z': fr,
                          'u': exp_data['Tv']})
figF, ax = figure()
error_prediction = model.predict(dataExp)
sy_error = []
for i in range(len(exp_data['sV'])):
    sy_error.append(error_prediction[i]*exp_data['sV'][i])
ax.fill_between(exp_data['muV'], exp_data['sV'], exp_data['sV']-np.array(sy_error), color='r', alpha=0.5, lw=0)
ax.plot(exp_data['muV'], exp_data['sV']-np.array(sy_error)/2., color='r', label='model-based\n correction', lw=1)
plot(exp_data['muV'], exp_data['sV'], sy=exp_data['ssV'], ax=ax)
ax.legend(frameon=False, loc=(0.3,-0.05), prop={'size':'x-small'}, handletextpad=1., handlelength=1.)
set_plot(ax, xlabel='$\mu_V$ (mV)', ylabel='$\sigma_V$ (mV)')
figF.savefig(desktop+'fig1.png', dpi=200)
figF.savefig(desktop+'fig1.svg')

from graphs.surface_plots import twoD_plot

sV0, muV0 = np.meshgrid(np.linspace(0.01, 0.2, 20), np.linspace(0.01, 0.65, 20))
dataModel=pandas.DataFrame({'x': muV0.flatten(), 
                            'y': sV0.flatten(),
                            'z': 0.1+0*muV0.flatten(),
                            'u': 30+0*muV0.flatten()})
error_prediction = model.predict(dataModel)
fig, ax, acb = figure_with_bar_legend(shift_up=0.35, shrink=.7)
acb2 = add_inset(ax, [1.1,-.15,.7,.07])
twoD_plot(muV0.flatten(), sV0.flatten(), error_prediction, colormap=PiYG,
          diverging=True, ax=ax, bar_legend=None, acb=None)
fr_bounds=[5e-2, 11.]
norm_fr = (np.log10(fr)-np.log10(fr_bounds[0]))/(np.log10(fr_bounds[1])-np.log10(fr_bounds[0]))
for i in range(len(fr)):
    ax.plot(norm_exp_muV[i], norm_exp_sV[i], 'o', ms=2, color=copper(norm_fr[i]))
set_plot(ax, xlabel='$\mu_V^{o}$ (a.u.)', ylabel='$\sigma_V^{o}$ (a.u.)',
         xlim=[muV0.min(), muV0.max()], ylim=[sV0.min(), sV0.max()], yticks=[0.1, 0.2])
build_bar_legend_continuous(acb, PiYG,
                            bounds=np.max(np.abs(DS[cond]))*np.array([-1,1]),
                            label='($\sigma_V^{o}-\sigma_V^t$)/$\sigma_V^{o}$')
build_bar_legend_continuous(acb2, copper,
                            bounds=fr_bounds,
                            ticks_labels=['0.1', '', '10'],
                            scale='log', orientation='horizontal',
                            label='$\\nu_{out}$ (Hz)', labelpad=0.)
acb2.set_title('exp. data', fontsize=FONTSIZE-1)
fig.savefig(desktop+'fig2.png', dpi=200)
fig.savefig(desktop+'fig2.svg')
show()
