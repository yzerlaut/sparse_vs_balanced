import numpy as np
import sys
sys.path.append('../')
from scipy.stats import skew

def OU_with_threshold(mu, sigma, tau, dt=0.1, tstop=2000, seed=1, threshold=1, reset=0):
    np.random.seed(seed)
    
    diffcoef = 2*sigma**2/tau
    y0 = mu
    n_steps = int(tstop/dt)
    A = np.sqrt(diffcoef*tau/2.*(1-np.exp(-2*dt/tau)))
    noise = np.random.randn(n_steps)
    y = np.zeros(n_steps)
    y[0] = 0
    nspike = 0
    for i in range(n_steps-1):
        y[i+1] = y0 + (y[i]-y0)*np.exp(-dt/tau)+A*noise[i]
        if y[i+1]>threshold:
            y[i+1] = reset
            nspike+=1
        elif y[i+1]<reset:
            y[i+1] = reset
    return y, 1e3*nspike/n_steps/dt

def plot1(y0, y1, mu, sigma, tv, fout, color='k'):

    fig, AX = figure(axes_extents=[[[4,1],[1,1]], [[4,1],[1,1]]], figsize=(.7, .5), bottom=.5, hspace=0.2, wspace=1.2, right=2.)
    axi = add_inset(AX[1][1], [1.6,.65,.6,1.])
    axi.bar([0], [sigma], color='r')
    axi.bar([1], [np.std(y1[np.arange(len(y0))>int(tv/0.1)])], color='k')
    axi.bar([3], [mu], color='r')
    axi.bar([4], [np.mean(y1[np.arange(len(y0))>int(tv/0.1)])], color='k')
    set_plot(axi, xticks=[0,1,3,4],
             xticks_labels=['$\sigma_V^{true}$     ', '$\sigma_V^{obs}$', '$\mu_V^{true}$      ', '$\mu_V^{obs}$'],
             xticks_rotation=90, fontsize=FONTSIZE-1)
    axi.set_title('(a.u.)')
    AX[0][0].plot(y0, '-', lw=1, color=Grey)
    draw_bar_scales(AX[0][0], (200,-0.1), 1000, '100ms', 0, '', orientation='right-bottom', fontsize=FONTSIZE)

    AX[1][0].plot(y1, 'k-', lw=1)
    for ax in [AX[1][0]]:
        ax.plot([0,len(y1)], [1,1], 'k--', lw=0.3)
        ax.plot([0,len(y1)], [0,0], 'k:', lw=0.3)
    AX[0][0].annotate('without boundaries', (.5,.86), xycoords='axes fraction', color=Grey)
    # AX[1][0].annotate('bounded LIF ', (.2, -.15), xycoords='axes fraction', color='k')
    AX[1][0].annotate('$\\nu_{output}$=%.1fHz' % fout, (.7, -.15), xycoords='axes fraction', color=color)
    hist(y0, orientation='vertical', c=Grey, ax=AX[0][1])
    AX[0][1].plot(yy, xx, lw=2, color=Red)
    hist(y1, orientation='vertical', c='k', ax=AX[1][1])
    AX[0][0].annotate('$\mu_V^{true}$=%.2f, $\sigma_V^{true}$=%.2f, $\\tau_V^{true}$=%.ims' % (mu, sigma, tv),
                      (.2, -0.1), xycoords='axes fraction', color=color)
    set_plot(AX[0][0], ['left'], ylabel='$V_m$ (a.u.)', xlim=[0,len(y1)])
    set_plot(AX[1][0], ['left'], ylabel='$V_m$ (a.u.)', xlim=[0,len(y1)], yticks=[0,1])
    set_plot(AX[0][1], ['left'])
    set_plot(AX[1][1], xticks=[], xlabel='n. hist', yticks=[0,1])
    return fig

from graphs.my_graph import *
DEMO = {'MU':np.array([.5, .93, .1, 1.]), 'SIGMA':np.array([.2, 0.04, 0.6, 0.1]),
        'TAU':[10, 5, 20, 40], 'SEED':[30, 40, 5, 20], 'COLOR':np.array([Red, Purple, Cyan, Orange])}

if sys.argv[-1]=='demo':
    from data_analysis.signal_library.classical_functions import gaussian

    for i in range(len(DEMO['MU'])):
        mu, sigma, tv = DEMO['MU'][i], DEMO['SIGMA'][i], DEMO['TAU'][i]
        xx = np.linspace(-3.*sigma, 3.*sigma, 1e2)+mu
        yy = gaussian(xx, mu, sigma)
        y0, _ = OU_with_threshold(mu, sigma, tv, threshold=100, reset=-1000, seed=DEMO['SEED'][i])
        y1, fout = OU_with_threshold(mu, sigma, tv, threshold=1, seed=DEMO['SEED'][i])
        fig = plot1(y0, y1, mu, sigma, tv, fout, color=DEMO['COLOR'][i])
        fig.savefig(desktop+'demo'+str(i)+'.svg')
    show()

elif sys.argv[-1]=='sim':

    N = 20
    MU, SIG, TV = np.linspace(0., 1., N), np.linspace(0.05, 1., N), [5., 10., 20., 40.]

    Mean, Sigma, TauV, Rec_Sigma, Rec_Mean, Skew, Fout = [], [], [], [], [], [], []

    for t, Tv in enumerate(TV):
        for i, mu in enumerate(MU):
            for j, s in enumerate(SIG):
                Mean.append(mu)
                Sigma.append(s)
                TauV.append(Tv)
                y1, fout = OU_with_threshold(mu, s, Tv, dt=0.1, tstop=1e3*Tv, threshold=1)
                Rec_Sigma.append(np.std(y1))
                Rec_Mean.append(np.mean(y1))
                Skew.append(skew(y1))
                Fout.append(fout)
                
    np.savez('mu_sigma_data.npz', **{'Mean':Mean, 'Sigma':Sigma, 'Skew':Skew, 'TauV':TauV,
                                     'Rec_Sigma':Rec_Sigma, 'Rec_Mean':Rec_Mean, 'Fout':Fout})
            
elif sys.argv[-1]=='plot':

    data = dict(np.load('mu_sigma_data.npz'))

    from graphs.surface_plots import twoD_plot
    from graphs.my_graph import *

    fig, AX = figure(axes=(len(np.unique(data['TauV'])),3), wspace=7., hspace=6., right=5.)
    DS = (data['Rec_Sigma']-data['Sigma'])/np.sqrt(data['Rec_Sigma']*data['Sigma'])
    # DS = (data['Rec_Sigma']-data['Sigma'])/data['Rec_Sigma']

    def bbl(i, acb):
        if i==0:
            build_bar_legend_continuous(acb, viridis, label='$\mu_V^o$ (a.u.)',
                                        ticks=[0.2,0.6,1.],
                                        bounds=[np.min(data['Rec_Mean']), np.max(data['Rec_Mean'])])
        elif i==1:
            build_bar_legend_continuous(acb, viridis, label='$\sigma_V^b$ (a.u.)',
                                        bounds=[np.min(data['Rec_Sigma']), np.max(data['Rec_Sigma'])])
        elif i==2:
            build_bar_legend_continuous(acb, viridis, label='$\\nu_{out}$ (Hz)', scale='log',
                                        bounds=[np.min(data['Fout']+1e-2), np.max(data['Fout']+1e-2)])
        elif i==3:
            build_bar_legend_continuous(acb, PiYG, label='($\sigma_V^{b}-\sigma_V^{t}$)/$\sqrt{\sigma_V^{b} \sigma_V^{t}}$',
                              bounds=np.max(np.abs((data['Rec_Sigma']-data['Sigma'])/data['Rec_Sigma']))*np.array([-1,1]))

    for i, t in enumerate(np.unique(data['TauV'])[::-1]):
        # AX[i][0].annotate('$\\tau_V$=%.1fms' % t, (1.,1.2), xycoords='axes fraction')
        for j, z, ax, title, zticks in zip(range(4), [data['Rec_Mean'],
                                                      data['Rec_Sigma'],
                                                      np.log10(data['Fout']+1e-2),
                                                      DS],
                                           AX[i],
                                           ['obs. $\mu_V$', 'obs. $\sigma_V$', '$\\nu_{out}$', '$\sigma_V$ error'],
                                           [[0.,1.], [0.2, .4, 0.6], [-2,0,2], [-1.5,0,1.5]]):
            acb = add_inset(ax, [1.1, .0, .07, 1.])
            bbl(j, acb)
            twoD_plot(data['Mean'][data['TauV']==t], data['Sigma'][data['TauV']==t], z[data['TauV']==t],
                      ax=ax, acb=None, bar_legend=None,
                      diverging=[False, False, False, True][j])
            ax.plot(DEMO['MU'][DEMO['TAU']==t], DEMO['SIGMA'][DEMO['TAU']==t], 'o', color=DEMO['COLOR'][DEMO['TAU']==t][0], ms=4)
            set_plot(ax, xlabel='$\mu_V^{true}$ (a.u.)', ylabel='$\sigma_V^{true}$ (a.u.)', yticks=[0.2, 0.6, 1],xlim=[-0.1,1.1],
                     xticks=[0, 0.5, 1.])
            ax.set_title(title)


    fig2, ax, acb = figure_with_bar_legend()
    for i, s in enumerate(np.unique(data['Sigma'])[::-1]):
        cond= (data['TauV']==10) & (data['Sigma']==s)
        ax.plot(data['Mean'][cond], data['Rec_Sigma'][cond], color=viridis(1.-i/(len(np.unique(data['Sigma']))-1)))
    build_bar_legend_continuous(acb, viridis,
                                bounds=[np.min(data['Sigma']), np.max(data['Sigma'])],
                                label='true $\sigma_V$ (a.u.)')
    set_plot(ax, xlabel='$\mu_V$ (a.u.)', ylabel='bounded $\sigma_V$ (a.u.)')
    fig.savefig(desktop+'fig.svg')
    fig2.savefig(desktop+'fig2.png', dpi=200)
    show()

else:
    from graphs.my_graph import *
    from data_analysis.signal_library.classical_functions import gaussian
    
    mu, sigma, tv = 0.7, 0.15, 10
    
    xx = np.linspace(-3.*sigma, 3.*sigma, 1e2)+mu
    yy = gaussian(xx, mu, sigma)
    y0, _ = OU_with_threshold(mu, sigma, tv, threshold=100)
    y1, fout = OU_with_threshold(mu, sigma, tv, threshold=1)
    
    fig = plot1(y0, y1, mu, sigma, tv, fout, color='k')
    
    fig.savefig(desktop+'fig.png', dpi=200)
    
    # fig, AX = figure(axes_extents=[[[4,1],[1,1]], [[4,1],[1,1]]], figsize=(.7, .5), bottom=.5, hspace=0.2, wspace=1.2, right=2.)
    # axi = add_inset(AX[1][1], [1.6,.6,.5,1.])
    # for i, sx, color in zip(range(3), [sigma, np.std(y0[np.arange(len(y0))>int(tv/0.1)]), np.std(y1)], [Red, Grey, 'k']):
    #     axi.bar([i], sx, color=color)
    # set_plot(axi, xticks=[0,1,2], xticks_labels=['theory', 'no $V_{thre}$', 'w. $V_{thre}$'], xticks_rotation=90)
    # axi.set_title('$\sigma_V$ (a.u.)     ')
    # AX[0][0].plot(y0, '-', lw=1, color=Grey)
    # draw_bar_scales(AX[0][0], (200,-0.1), 1000, '100ms', 0, '', orientation='right-bottom', fontsize=FONTSIZE)

    # AX[1][0].plot(y1, 'k-', lw=1)
    # for ax in [AX[1][0]]:
    #     ax.plot([0,len(y1)], [1,1], 'k--', lw=0.3)
    #     ax.plot([0,len(y1)], [-1,-1], 'k:', lw=0.3)
    # AX[1][0].annotate('$\\nu_{output}$=%.1fHz' % fout, (.7, -.15), xycoords='axes fraction', color='k')
    # hist(y0, orientation='vertical', c=Grey, ax=AX[0][1])
    # AX[0][1].plot(yy, xx, lw=2, color=Red)
    # hist(y1, orientation='vertical', c='k', ax=AX[1][1])
    # AX[0][0].annotate('$\mu_V$=%.1f, $\sigma_V$=%.1f, $\\tau_V$=%.ims' % (mu, sigma, tv), (.5, -0.1), xycoords='axes fraction', color=Grey)
    # set_plot(AX[0][0], ['left'], ylabel='$V_m$ (a.u.)', xlim=[0,len(y1)])
    # set_plot(AX[1][0], ['left'], ylabel='$V_m$ (a.u.)', xlim=[0,len(y1)], yticks=[-1,0,1])
    # set_plot(AX[0][1], ['left'])
    # set_plot(AX[1][1], xticks=[], xlabel='n. hist', yticks=[-1,0,1])
    # fig.savefig(desktop+'fig.png', dpi=200)
    show()
            


    
