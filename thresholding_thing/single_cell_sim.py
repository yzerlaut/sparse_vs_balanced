import numpy as np
import os, sys
sys.path.append('../')
from graphs.my_graph import *
from model import Model, pass_arguments_of_new_model
from scipy.stats import skew

ntwk_sim = dict(np.load('../sparse_vs_balanced/data/dAE_dsnh_aff_coupling_0.075_analyzed.npz'))
Fa, Fe, Fi = ntwk_sim['faff'].mean(axis=1), ntwk_sim['mean_exc'].mean(axis=1), ntwk_sim['mean_inh'].mean(axis=1)

Model2 = Model.copy()
Model2['POP_STIM'] = ['RecExc', 'RecInh', 'AffExc']
Model2['NRN_KEY'] = 'RecExc'
Model2['N_SEED'] = 4
Model2['tstop'] = 1000.
Model2['dt'] = 0.1

def evaluate_Vm_fluctuations(data,
                             t_init=100, repolarization_blank=0, Trefrac=5):
    t = np.arange(len(data['Vm'][0,:]))*Model2['dt']
    muV, sV, gV, Tv = [], [], [], []
    for i in range(data['Vm'].shape[0]):
        tspikes = data['tspikes'][np.argwhere(data['ispikes']==i).flatten()]
        cond = (t>t_init)
        for tt in tspikes:
            cond2 = (t>tt-repolarization_blank) & (t<tt+Trefrac+repolarization_blank)
            cond = cond & np.invert(cond2)
        muV.append(np.mean(data['Vm'][i,:][cond]))
        sV.append(np.std(data['Vm'][i,:][cond]))
        gV.append(skew(data['Vm'][i,:][cond]))
        Tv.append(0)
    return np.array(muV), np.array(sV), np.array(gV), np.array(Tv)

def Vm_hist(Vm, tspikes, ispikes,
            with_Vm = False,
            bins = np.linspace(-75, -45, 30),
            t_init=100, repolarization_blank=0., Trefrac=5):
    t = np.arange(len(Vm[0,:]))*Model2['dt']
    VM = np.empty(0)
    VMS = []
    for i in range(Vm.shape[0]):
        ttspikes = tspikes[np.argwhere(ispikes==i).flatten()]
        cond = (t>t_init)
        for tt in ttspikes:
            cond2 = (t>tt-repolarization_blank) & (t<tt+Trefrac+repolarization_blank)
            cond = cond & np.invert(cond2)
        VM = np.concatenate([VM, Vm[i,:][cond]])
        if with_Vm:
            VMS.append({'t':t[cond], 'Vm':Vm[i,:][cond]})
    print(np.std(VM))
    hist, be = np.histogram(VM, bins=bins, normed=True)
    if with_Vm:
        return VMS, .5*(be[1:]+be[:-1]), hist
    else:
        return .5*(be[1:]+be[:-1]), hist

def run_with_and_without_thresh(fa, fe, fi, with_hist=False, N=4):
    """
    """
    Model2['N_SEED'] = N
    output = {}
    SYN_POPS = build_up_afferent_synaptic_input(Model2, ['RecExc', 'RecInh', 'AffExc'], NRN_KEY='RecExc')
    Model2['RATES'] = {'F_RecExc':fe, 'F_RecInh':fi, 'F_AffExc':fe}
    
    Model2['RecExc_Vthre'] = Model['RecExc_Vthre'] # real value 
    data1 = run_sim(Model2, firing_rate_only=False, with_Vm=Model2['N_SEED'])
    output['fout'], output['sfout'] = data1['fout_mean'], data1['fout_std']
    output['muV'], output['sV'], output['gV'], output['Tv'] = evaluate_Vm_fluctuations(data1)
    
    Model2['RecExc_Vthre'] = 1000. # no threshold
    data2 = run_sim(Model2, firing_rate_only=False, with_Vm=Model2['N_SEED'])
    output['muV_subthre'], output['sV_subthre'], output['gV_subthre'], output['Tv_subthre'] = evaluate_Vm_fluctuations(data2)

    return output, data1, data2

    
i1, i2, i3 = 9, 13, 27

if sys.argv[-1]=='demo':

    Model2['tstop'] = 3000.
    ## RUN SIM
    # from neural_network_dynamics.transfer_functions.single_cell_protocol import run_sim, my_logspace
    # from neural_network_dynamics.cells.cell_library import built_up_neuron_params
    # from neural_network_dynamics.theory.tf import build_up_afferent_synaptic_input
    # from neural_network_dynamics.transfer_functions.single_cell_protocol import run_sim, my_logspace
    # from neural_network_dynamics.cells.cell_library import built_up_neuron_params
    # OUTPUT = {}
    # for i in [i1, i2, i3]:
    #     print(Fa[i], Fe[i], Fi[i])
    #     output, data1, data2 = run_with_and_without_thresh(Fa[i], Fe[i], Fi[i], N=4)
    #     OUTPUT['Vm_subthre_'+str(i)] = data2['Vm']
    #     OUTPUT['ispikes_subthre_'+str(i)] = data2['ispikes']
    #     OUTPUT['tspikes_subthre_'+str(i)] = data2['tspikes']
    #     OUTPUT['Vm_'+str(i)] = data1['Vm']
    #     OUTPUT['ispikes_'+str(i)] = data1['ispikes']
    #     OUTPUT['tspikes_'+str(i)] = data1['tspikes']
    # np.savez('Vm_data.npz', **OUTPUT)
    
    ## PLOT SIM
    OUTPUT = np.load('Vm_data.npz')
    fig, AX = figure(axes=(2,3), figsize=(1., .7))
    fig2, AX2 = figure(axes=(2,1), figsize=(1., 1.), hspace=3.)
    for ax1, ax2, i, j, c in zip(AX[0], AX[1], [i1, i2, i3], [2, 0, 2], [Blue, Grey, Orange]):
        Vms = OUTPUT['Vm_subthre_'+str(i)][j,:]
        Vm = OUTPUT['Vm_'+str(i)][j,:]
        t= np.arange(len(Vm))*Model2['dt']
        condVm = t<1300
        ax1.set_title('$\\nu_a$=%.1f  ($\\nu_e$=%.1f, $\\nu_i$=%.1f)' % (Fa[i], Fe[i], Fi[i]), fontsize=FONTSIZE-1)
        ax2.plot(t[condVm], Vms[condVm], color=c, lw=1)
        ax1.plot(t[condVm], Vm[condVm], color=c, lw=1)
        VMS, be, hist = Vm_hist(OUTPUT['Vm_'+str(i)], OUTPUT['tspikes_'+str(i)], OUTPUT['ispikes_'+str(i)], with_Vm=True)
        # ax1.plot(VMS[j]['t'], VMS[j]['Vm'], color=c, lw=1)
        AX2[0].plot(be, hist, color=c)
        be, hist = Vm_hist(OUTPUT['Vm_subthre_'+str(i)], OUTPUT['tspikes_subthre_'+str(i)], OUTPUT['ispikes_subthre_'+str(i)])
        # cond = (OUTPUT['ispikes_'+str(i)]==j) & (OUTPUT['tspikes_'+str(i)]<200)
        # ax1.plot(OUTPUT['tspikes_'+str(i)][cond], -50+0*OUTPUT['tspikes_'+str(i)][cond], 'ko', ms=3)
        AX2[1].plot(be, hist, color=c)
        set_plot(ax1, ylim=[-75, -45])
        set_plot(ax2, ylim=[-75, -45])
    set_plot(AX2[0], xlabel='$V_m$ (mV)', ylabel='n. hist')
    set_plot(AX2[1], xlabel='$V_m$ (mV)', ylabel='n. hist')
    AX[0][0].set_ylabel('Vm (mV)')
    AX[1][0].set_ylabel('Vm (mV)')
    AX[1][0].set_xlabel('time (s)')
    from graphs.plot_export import put_list_of_figs_to_svg_fig
    put_list_of_figs_to_svg_fig([fig, fig2], desktop+'threshold.svg')
    # show()
    
elif sys.argv[-1]=='run':
    
    from neural_network_dynamics.transfer_functions.single_cell_protocol import run_sim, my_logspace
    from neural_network_dynamics.cells.cell_library import built_up_neuron_params
    from neural_network_dynamics.theory.Vm_statistics import getting_statistical_properties
    from neural_network_dynamics.theory.tf import build_up_afferent_synaptic_input
    
    Model2['tstop'] = 1000.
    N = 4
    final_output = {}
    for key in ['fout', 'sfout', 'muV', 'sV', 'gV', 'Tv',\
                'muV_subthre', 'sV_subthre', 'gV_subthre', 'Tv_subthre']:
        final_output[key] = []
    Model2['N_SEED'] = N
    for i in range(Fa.shape[0]):
        output, _, _ = run_with_and_without_thresh(Fa[i], Fe[i], Fi[i])
        for key in ['fout', 'sfout', 'muV', 'sV', 'gV', 'Tv',\
                    'muV_subthre', 'sV_subthre', 'gV_subthre', 'Tv_subthre']:
            final_output[key].append(output[key])
    for key in ['fout', 'sfout', 'muV', 'sV', 'gV', 'Tv',\
                'muV_subthre', 'sV_subthre', 'gV_subthre', 'Tv_subthre']:
        final_output[key] = np.array(final_output[key])
    np.savez('../sparse_vs_balanced/data/with_without_threshold.npz', **final_output)
    
elif sys.argv[-1]=='plot':
    data = dict(np.load('../sparse_vs_balanced/data/with_without_threshold.npz'))
    fig, ax = figure()
    fig2, ax2 = figure()
    fig3, ax3 = figure()
    print(data['sV_subthre'])
    cond = (Fa>5.) & (Fa<23.)
    sV, ssV = data['sV_subthre'].mean(axis=1), data['sV_subthre'].std(axis=1)
    muV, smuV = data['muV_subthre'].mean(axis=1), data['muV_subthre'].std(axis=1)
    plot(Fa[cond], sV[cond], sy=ssV[cond], color='r', label='no thresh.', ax=ax)
    plot(Fa[cond], muV[cond], sy=smuV[cond], color='r', label='no thresh.', ax=ax2)
    plot(muV[cond], sV[cond], sy=ssV[cond],color='r', label='no thresh.', ax=ax3)
    sV, ssV = data['sV'].mean(axis=1), data['sV'].std(axis=1)
    muV, smuV = data['muV'].mean(axis=1), data['muV'].std(axis=1)
    plot(Fa[cond], sV[cond], sy=ssV[cond], color='k', label='with thresh.', ax=ax)
    plot(Fa[cond], muV[cond], sy=smuV[cond], color='k', label='with thresh.', ax=ax2)
    plot(muV[cond], sV[cond], sy=ssV[cond], color='k', label='with thresh.', ax=ax3)
    set_plot(ax, xscale='log', xlim=[4.9,25], xticks=[5, 10, 20], xticks_labels=['5', '10', '20'],
             xlabel='$\\nu_a$ (Hz)', ylabel='$\sigma_V$ (mV)')
    set_plot(ax2, xscale='log', xlim=[4.9,25], xticks=[5, 10, 20], xticks_labels=['5', '10', '20'],
             xlabel='$\\nu_a$ (Hz)', ylabel='$\mu_V$ (mV)')
    set_plot(ax3, ylabel='$\sigma_V$ (mV)', xlabel='$\mu_V$ (mV)')
    for ii, c in zip([i1, i2, i3], [Blue, 'k', Orange]):
        ax.plot([Fa[ii]], [sV[ii]], 'o', color=c, ms=5, alpha=.5)
        ax2.plot([Fa[ii]], [muV[ii]], 'o', color=c, ms=5, alpha=.5)
        ax3.plot([muV[ii]], [sV[ii]], 'o', color=c, ms=5, alpha=.5)
    ax.legend(frameon=False, prop={'size':'x-small'}, handletextpad=0.2, handlelength=1.)
    ax2.legend(frameon=False, prop={'size':'x-small'}, handletextpad=0.2, handlelength=1.)
    ax3.legend(frameon=False, prop={'size':'x-small'}, handletextpad=0.2, handlelength=1.)
    from graphs.plot_export import put_list_of_figs_to_svg_fig
    put_list_of_figs_to_svg_fig([fig, fig2, fig3], desktop+'fig.svg')
    # show()
