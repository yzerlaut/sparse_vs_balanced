import numpy as np
from itertools import product
import sys, pathlib, os
sys.path.append(str(pathlib.Path(__file__).resolve().parents[1]))
import neural_network_dynamics.main as ntwk
from data_analysis.IO.hdf5 import load_dict_from_hdf5
from graphs.my_graph import *
# everything stored within a zip file
import zipfile
from scipy.special import erf
from data_analysis.processing.signanalysis import gaussian_smoothing
import matplotlib.cm as cm
from graphs.plot_export import put_list_of_figs_to_svg_fig
from sparse_vs_balanced.running_3pop_model import run_3pop_ntwk_model
import pymuvr # for spike train metrics
from sklearn.neighbors import KNeighborsClassifier

def one_Vm_fig(data, Model,
               iNVm_Exc=np.arange(3), iNVm_Inh=0, iNVm_DsInh=0,
               dV=30, vpeak=-43, smoothing=10e-3, Gl=10,
               FIGSIZE=(6,4),
               tstart=0., tend=1e6, subsampling=20,
               blank_spike=6., ):

    t = np.arange(len(data['VMS_RecExc'][0]))*Model['dt']
    
    fig1, ax = plt.subplots(1, figsize=FIGSIZE)
    plt.subplots_adjust(top=.99, bottom=.2, left=.25, right=.99)
    # exc
    for i, k in enumerate(iNVm_Exc):
        cond = (t>tstart) & (t<tend)
        tspikes = data['tRASTER_RecExc'][np.argwhere(data['iRASTER_RecExc']==k).flatten()]
        for ts in tspikes[tspikes>tstart]:
            ax.plot([ts, ts],
                    [data['RecExc_Vthre']+i*dV, vpeak+i*dV], '--', lw=1, color=Green)
            cond = cond & np.invert((t>=ts-2.*Model['dt']) & (t<=(ts+blank_spike)))
            ispike = int(ts/data['dt'])
        ax.plot(t[cond][::subsampling], data['VMS_RecExc'][k][cond][::subsampling]+i*dV, '-', lw=1, color=Green)

    # inh
    tspikes = data['tRASTER_RecInh'][np.argwhere(data['iRASTER_RecInh']==iNVm_Inh).flatten()]
    cond = (t>tstart) & (t<tend)
    for ts in tspikes[tspikes>tstart]:
        ax.plot([ts, ts], [data['RecInh_Vthre']-dV, vpeak-dV], '--', color=Red, lw=1)
        cond = cond & np.invert((t>=ts-2.*Model['dt']) & (t<=(ts+blank_spike)))
    ax.plot(t[cond][::subsampling],
            data['VMS_RecInh'][iNVm_Inh][cond][::subsampling]-dV, '-', lw=1, color=Red)
    
    # dsinh
    cond = (t>tstart) & (t<tend)
    tspikes = data['tRASTER_DsInh'][np.argwhere(data['iRASTER_DsInh']==iNVm_DsInh).flatten()]
    for ts in tspikes[tspikes>tstart]:
        ax.plot([ts, ts],
                [data['DsInh_Vthre']-2*dV, vpeak-2*dV], '--', color=Purple, lw=1)
        cond = cond & np.invert((t>=ts-2.*Model['dt']) & (t<=(ts+blank_spike)))
    ax.plot(t[cond][::subsampling],
            data['VMS_DsInh'][iNVm_DsInh][cond][::subsampling]-2*dV, '-', lw=1, color=Purple)
    
    ax.plot([0,0], [-70, -60], 'k-')
    ax.annotate('10 mV', (0, -55))
    ax.plot([0,1000], [-50, -50], 'k-')
    ax.annotate('1s', (0, -55))
    set_plot(ax, [], yticks=[], xticks=[], xlim=[t[cond][0], t[cond][-1]])
    
    return fig1

def pop_act_time_course(data, Model,
                        tstart=0., tend=1e6,
                        smoothing=20,
                        FIGSIZE=(6,1.5),
                        lower_lim=0.01,
                        YTICKS=[0.01, 0.1, 1., 10.],
                        YTICKS_LABELS=['<0.01', '0.1', '1', '10']):


    
    t = np.arange(len(data['VMS_RecExc'][0]))*Model['dt']
    
    ismooth = int(smoothing/Model['dt'])
    
    cond = (t>tstart) & (t<tend)
    
    fig3, ax = plt.subplots(1, figsize=FIGSIZE)
    plt.subplots_adjust(top=.99, bottom=.2, left=.25, right=.99)

    for key, label, color in zip(['POP_ACT_RecInh', 'POP_ACT_RecExc', 'POP_ACT_DsInh'],
                                 ['$\\nu_i$', '$\\nu_e$', '$\\nu_d$'],
                                 [Red, Green, Purple]):
        y = gaussian_smoothing(data[key], ismooth)
        y[y<lower_lim] = 1.05*lower_lim
        ax.plot(t[cond], y[cond], color=color, label=label, lw=1)

    ax.set_yscale('log')
    ax.legend(frameon=False)
    set_plot(ax, ['left'],
             yticks=YTICKS, yticks_labels=YTICKS_LABELS, 
             ylabel='rate (Hz)', xticks=[], xlim=[t[cond][0], t[cond][-1]])
    return fig3


def additional_spikes_func(pre_pop, target_pop, Model):
    if (target_pop=='RecExc') & (pre_pop=='AffExc'):
        additional_spikes={}
        additional_spikes['indices'],\
            additional_spikes['times'] = \
            ntwk.translate_aff_spikes_into_syn_target_events(np.array(Model['pre_indices'], dtype=int),
                                                             Model['pre_times'],
                                                             Model['M_SOURCE_TARGET'])
    else:
        additional_spikes={'indices':[], 'times':[]}
    return additional_spikes

def complex_presynaptic_pattern(Model, time_array,
                                Pattern_Seed=13,
                                Bg_Act_Seed=12,
                                afferent_pop='AffExc',
                                target_pop='RecExc'):
    
    np.random.seed(Pattern_Seed) # setting the seed !

    N_independent = int(Model['N_source']/Model['N_duplicate'])
    N_source = N_independent*Model['N_duplicate'] # N_source needs to be a multiple of N_duplicate

    DUPLICATION_MATRIX = np.array([\
                        np.random.choice(np.arange(N_source), Model['N_duplicate'], replace=False)\
                             for k in range(N_independent)])
    
    pre_indices, pre_times = [], []

    dt = (time_array[1]-time_array[0])

    EVENTS=np.sort(np.random.uniform(Model['time_span'],\
                                     size=Model['N_event']))
    COACT = np.random.randint(N_independent, size=Model['N_event'])

    # here we change the seed to have it 
    np.random.seed(int(Pattern_Seed*(Bg_Act_Seed+3))%178) # setting the seed !
    ACTIVS = np.arange(int(Model['tstop']/Model['tswitch']))[1:]*Model['tswitch']+Model['T_around']
    ACTIVS = ACTIVS+np.random.uniform((Model['tswitch']-Model['time_span']-2*Model['T_around']), size=len(ACTIVS))

    for a in ACTIVS:
        for e, c in zip(EVENTS, COACT):
            for jj in DUPLICATION_MATRIX[c, :]:
                pre_indices.append(jj)
                pre_times.append(e+a) # (to be done) add a random time shift here

    M_SOURCE_TARGET = ntwk.build_aff_to_pop_matrix('AffExc', 'RecExc', Model,
                                                   N_target_pop=Model['N_target'],
                                                   SEED=Pattern_Seed+34) # here the seed is fixed by the pattern
    return pre_indices, pre_times, ACTIVS, M_SOURCE_TARGET


def drowsy_rhythm(t, Model):
    # return np.sin(2.*np.pi*Model['low_freq_rhythm']*t)**2
    return np.sin(2.*np.pi*2e-3*t)**2


def envelope(arousal_index):
    return 12*np.exp(-arousal_index/.15), 20*np.exp(-(1.-arousal_index)/.3)


def arousal_func(t, Model):
    return (np.floor((t-Model['tswitch'])/Model['tswitch'])%Model['N_levels'])/(Model['N_levels']-1)


def final_func(t, Model):
    arousal_index = arousal_func(t, Model)
    rcomp, scomp = envelope(arousal_index)
    return rcomp*drowsy_rhythm(t, Model)+scomp


def spike_train_distance(PATTERNS, cos=0.1, tau=5):
    """
    A distance based on the Multi-Unit Van Rossum metrics
    """
    return pymuvr.square_dissimilarity_matrix(PATTERNS,\
                                              cos, tau, 'distance')[0,1]


def run_sim(Model, verbose=False,
            KEY_NOT_TO_RECORD=['iRASTER_PRE', 'POP_ACT', 'VMS', 'iRASTER_PRE_in_terms_of_Pre_Pop'],
            with_pop_act=False):

    Model['tstop'] = (Model['N_levels']*Model['N_repeat']+1)*Model['tswitch']
    
    Model['pre_indices'],\
     Model['pre_times'],\
      Model['ACTIVS'],\
       Model['M_SOURCE_TARGET'] = complex_presynaptic_pattern(Model,
                                        np.arange(int(Model['tstop']/Model['dt']))*Model['dt'],
                                                              Pattern_Seed=Model['Pattern_Seed'],
                                                              Bg_Act_Seed=Model['Bg_Act_Seed'])
    Model['Arousal_Val'] = np.array([arousal_func(a, Model) for a in Model['ACTIVS']])

    NTWK = run_3pop_ntwk_model(Model, tstop=Model['tstop'],
                               filename=Model['filename'],
                               with_Vm=Model['N_Vm'],
                               with_pop_act=with_pop_act,
                               with_synaptic_currents=False,
                               with_synaptic_conductances=False,
                               additional_spikes_func=additional_spikes_func,
                               faff_waveform_func=final_func,
                               BG_ACT_SEED=Model['Bg_Act_Seed'],
                               CONNEC_SEED=(Model['Connec_Seed']+3*Model['Bg_Act_Seed'])%18,
                               KEY_NOT_TO_RECORD=KEY_NOT_TO_RECORD,
                               verbose=verbose)

    return NTWK

def run_scan(Model, fix_missing_only=False):

    Model['N_Vm'] = 0 # removing the Vm recordings
    ntwk.run_scan(Model,
                 ['Bg_Act_Seed', 'Pattern_Seed'],
                  [np.arange(Model['N_trials'])+3,
                   np.arange(Model['N_patterns'])+37],
                  run_sim, fix_missing_only=fix_missing_only)

def construct_response_array(zip_filename, T_around=100):        

    Model, PARAMS_SCAN, DATA = ntwk.get_scan({}, filename=zip_filename)

    FILENAMES = np.array(PARAMS_SCAN['FILENAMES'])
    Pattern_Seeds = np.array(PARAMS_SCAN['Pattern_Seed'])
    Bg_Act_Seeds = np.array(PARAMS_SCAN['Bg_Act_Seed'])
    
    # we construct the data structure: PATTERNS[patternID][arousalID][trialID]
    PATTERNS = []
    # first loop over the presynaptic pattern
    for p in range(int(Model['N_patterns'])):
        PATTERNS.append([]) # a new set of response patterns 
        for a in range(int(Model['N_levels'])):
            PATTERNS[p].append([]) # a new set of patterns for that arousal level
            
    # first loop over the presynaptic pattern
    for p, P in enumerate(np.unique(Pattern_Seeds)):
        # then loop over the different trials (varying backgroung)
        for bg in np.unique(Bg_Act_Seeds):
            # then loop over the ~arousal levels
            i0 = np.arange(len(FILENAMES))[(Pattern_Seeds==P) & (Bg_Act_Seeds==bg)][0]
            arousal, activations = DATA[i0]['Arousal_Val'], DATA[i0]['ACTIVS']
            times, indices = DATA[i0]['tRASTER_RecExc'], DATA[i0]['iRASTER_RecExc']
            for a, A in enumerate(np.linspace(0,1,Model['N_levels'])):
                for tt in activations[np.round(arousal,5)==np.round(A,5)]:
                    cond = (times>tt-T_around) & (times<tt+Model['time_span']+T_around) & (indices<=Model['N_target'])
                    RASTER = [] # let's construct the right format
                    for ii in np.arange(int(Model['N_target'])):
                        Nrn = []
                        i0 = np.argwhere(indices[cond]==ii).flatten()
                        Nrn += list(times[cond][i0].flatten()-tt)
                        RASTER.append(Nrn)
                    PATTERNS[p][a].append(RASTER)

    return Model, PARAMS_SCAN, FILENAMES, Pattern_Seeds, Bg_Act_Seeds, PATTERNS


def plot_spiking_resp_single_trial(Model0):

    Model, PARAMS_SCAN,\
        FILENAMES, Pattern_Seeds,\
        Bg_Act_Seeds, PATTERNS = construct_response_array(Model0['zip_filename'],
                                                          T_around=Model0['T_around'])

    if len(Model0['Nnrn_Vis'])==int(Model['N_levels']):
        Nnrn_Vis = Model0['Nnrn_Vis']
    else:
        Nnrn_Vis = np.ones(int(Model['N_levels']), dtype=int)*int(Model['N_source'])
        
    FIGS = []
    for ps in range(int(Model['N_patterns'])):
        print('pattern level: ', ps)
        # one fig per pattern
        fig, AX = plt.subplots(int(Model['N_levels'])+1, figsize=(6,8))

        # getting the response
        for a, ax in zip(range(int(Model['N_levels'])), AX[1:]):
            for i in Model0['Trial_Vis']:
                for k in range(Nnrn_Vis[a]):
                    ax.plot( PATTERNS[ps][a][i][k], k*np.ones(len(PATTERNS[ps][a][i][k])),
                             'o', color=viridis(i/2.), ms=4, alpha=.5)
            ax.plot(-Model0['T_around']*np.ones(2)+1, Nnrn_Vis[a]-np.arange(2)*Model0['Nbar'],
                    'k-', lw=2)
            ax.annotate(str(Model0['Nbar'])+'cells', (-Model0['T_around']+1,Nnrn_Vis[a]),fontsize=13)
            set_plot(ax, [], yticks=[], xticks=[],
                     ylabel='AI='+str(round(np.linspace(0,1,int(Model['N_levels']))[a],2)),
                     xlim=[-Model0['T_around'],Model['time_span']+Model0['T_around']])
            
        # now plotting the input
        Model['tstop'] = (Model['N_levels']*Model['N_repeat']+1)*Model['tswitch']
        pre_indices,\
         pre_times,\
          ACTIVS, _ = complex_presynaptic_pattern(Model,
                                    np.arange(int(Model['tstop']/Model['dt']))*float(Model['dt']),
                                    Pattern_Seed=np.unique(Pattern_Seeds)[ps],
                                    Bg_Act_Seed=int(Model['Bg_Act_Seed']))
        t0 = ACTIVS[0]
        cond = (pre_times>t0-Model0['T_around']) &\
               (pre_times<t0+Model['time_span']+Model0['T_around'])
        AX[0].plot([-Model0['T_around'],Model['time_span']+Model0['T_around']],
                   [0,Model['N_source']], 'w.')
        AX[0].plot([-Model0['T_around']+10, -Model0['T_around']+110], [10,10], 'k-')
        AX[0].annotate('100ms', (-Model0['T_around']+30,13),fontsize=13)
        AX[0].plot(np.array(pre_times)[cond]-t0, np.array(pre_indices)[cond], 'ko', ms=3)
        AX[0].plot(-Model0['T_around']*np.ones(2)+1, Model['N_source']-np.arange(2)*Model0['Nbar'],
                    'k-', lw=2)
        AX[0].annotate(str(Model0['Nbar'])+'cells', (-Model0['T_around']+1,Model['N_source']),
                       fontsize=13)
        set_plot(AX[0], [], yticks=[], xticks=[],
                 xlim=[-Model0['T_around'],Model['time_span']+Model0['T_around']])

        ax = fig.add_axes([0.91, 0.8, 0.02, 0.05])
        build_bar_legend(np.arange(len(Model0['Trial_Vis'])+1), ax, viridis,
                         label='trial #', ticks_labels=[])

        
        FIGS.append(fig)
        
    return FIGS

                    
    
def decoding_analysis(zip_filename, N_training=1, T_around=100):

    Model, PARAMS_SCAN,\
        FILENAMES, Pattern_Seeds,\
        Bg_Act_Seeds, PATTERNS = construct_response_array(zip_filename,
                                                          T_around=T_around)

    if N_training>=Model['N_trials']:
        N_training = Model['N_trials']-1
        print('Only ', Model['N_trials'], 'trials available, so N_training is set to', N_training)
            
    Decoding_Accuracy = np.zeros((int(Model['N_levels']), int(Model['N_patterns'])))
    
    for a in range(int(Model['N_levels']))[:-1]:

        def my_pymuvr_metric(X1, X2):
            """
            Desiging a custom 'metric' function that fits the datatype of 'sklearn.neighbors.KNeighborsClassifier'

            the first N_training samples are the training data...
            N_training and Patterns are global variables :(
            """
            pattern1, flag1, trial1 = np.array(X1, dtype=int)
            pattern2, flag2, trial2 = np.array(X2, dtype=int)
            if (flag1==0) and (trial1>=N_training):
                print('problem in the indexing !')
            if (flag2==0) and (trial2>=N_training):
                print('problem in the indexing !')
            # constructing RASTER1
            if flag1==1: RASTER1 = PATTERNS[pattern1][a][N_training+trial1] # test flag
            elif flag1==0: RASTER1 = PATTERNS[pattern1][a][trial1] # training flag
            else: RASTER1 = None # this will raise an error
            # constructing RASTER2
            if flag2==1: RASTER2 = PATTERNS[pattern2][a][N_training+trial2] # test flag
            elif flag2==0: RASTER2 = PATTERNS[pattern2][a][trial2] # training flag
            else: RASTER2 = None # this will raise an error

            return spike_train_distance([RASTER1, RASTER2])

        
        neigh = KNeighborsClassifier(n_neighbors=1, metric=my_pymuvr_metric)
        TRAINING_ARRAY = np.array([[p, 0, i]\
                                   for p, i in product(np.arange(Model['N_patterns']),
                                                      np.arange(int(N_training)))])
        TRAINING_FLAG = np.array([p \
                                   for p, i in product(np.arange(Model['N_patterns']),
                                                      np.arange(int(N_training)))])
        neigh.fit(TRAINING_ARRAY, TRAINING_FLAG)
        # print('TRAINING_ARRAY')
        # print(TRAINING_ARRAY)
        # print('TRAINING_FLAG')
        # print(TRAINING_FLAG)

        TEST_ARRAY = np.array([[p, 1, i] \
                               for p, i in product(np.arange(int(Model['N_patterns'])),
                                                      np.arange(int(Model['N_trials']-N_training)))])
        print('TEST_ARRAY')
        print(TEST_ARRAY)
        TEST_FLAG = np.array([p \
                              for p, i in product(np.arange(Model['N_patterns']),
                                                     np.arange(int(Model['N_trials']-N_training)))])
        print('TEST_FLAG, a=', a)
        print(TEST_FLAG)
        TEST_AROUSAL = np.array([a\
                                 for p, i in product(np.arange(int(Model['N_patterns'])),
                                                     np.arange(int(Model['N_trials']-N_training)))])
        PREDICTED_FLAG = np.array(neigh.predict(TEST_ARRAY))
        print(PREDICTED_FLAG)
        for p in range(int(Model['N_patterns'])):
            cond = (TEST_FLAG==p) & (TEST_AROUSAL==a)
            Decoding_Accuracy[a,p] = np.sum(PREDICTED_FLAG[cond]==TEST_FLAG[cond])*100./len(TEST_FLAG[cond])
        
    fig2, ax3 = plt.subplots(figsize=(4,3))
    plt.subplots_adjust(left=.4, bottom=.3)
    ax3.plot([0,1], [0,100], 'w.')
    ax3.plot(np.linspace(0,1,Model['N_levels']), Decoding_Accuracy.mean(axis=1), 'k-')
    ax3.fill_between(np.linspace(0,1,Model['N_levels']),
                     Decoding_Accuracy.mean(axis=1)+Decoding_Accuracy.std(axis=1),
                     Decoding_Accuracy.mean(axis=1)-Decoding_Accuracy.std(axis=1),
                     color='lightgray', lw=0)
    set_plot(ax3, ylabel='decoding\naccuracy (%)', xlabel='~arousal index', yticks=[0,50,100], xticks=np.arange(3)*.5)
    return [fig2]
    
def show_input(Model):

    Model['tstop'] = (Model['N_levels']*Model['N_repeat']+1)*Model['tswitch']
    t = np.arange(int(Model['tstop']/Model['dt']))*Model['dt']
    fig, [ax1, ax] = plt.subplots(2, figsize=(4.5,3))
    plt.subplots_adjust(right=.99)
    ax.plot(t, final_func(t, Model), 'k-', lw=1)
    ax.plot([0,1000], [5,5], 'k-');ax.annotate('1s', (0,6))
    # # presynaptic activations
    _, _, activations, _ = complex_presynaptic_pattern(Model, t,
                                                       Pattern_Seed=Model['Pattern_Seed'],
                                                       Bg_Act_Seed=Model['Bg_Act_Seed'])
    set_plot(ax, ['left'], ylabel='$\\nu_a$ (Hz)', xticks=[], num_yticks=3, xlim=[0, Model['tstop']])
    ax1.plot(t, arousal_func(t, Model), 'k-', lw=1)
    for a in activations:
        ax1.plot([a+Model['time_span']/2.], [1], 'kv')
        ax1.fill_between([a, a+Model['time_span']], [0,0], [1, 1], alpha=.1, color='k', lw=0)
    set_plot(ax1, ['left'], ylabel='~arousal\n index', xticks=[], yticks=[0,.5,1], xlim=[0, Model['tstop']])
    fig2, ax0 = plt.subplots(figsize=(3,2.5))
    plt.subplots_adjust(left=.4, bottom=.3)
    arousal_levels = np.linspace(0,1)
    ax0.plot(arousal_levels, envelope(arousal_levels)[0], 'C4', lw=3, label='rhythmic')
    ax0.plot(arousal_levels, envelope(arousal_levels)[1], 'C5', lw=3, label='static')
    set_plot(ax0, yticks=np.arange(4)*6, xticks=np.arange(3)*.5,
             xlabel='~ arousal index', ylabel='$\\nu_a$ term\namp. (Hz)')
    ax0.legend(frameon=False)
    ax = plt.axes([0.55, .7,.2,.2])
    ax.plot(drowsy_rhythm(np.arange(1000), None), color='C4')
    ax.plot([0,400], [.7,.7], 'k-', lw=1)
    ax.annotate('400ms', (20,.8))
    set_plot(ax, [], yticks=[], xticks=[])
    return [fig, fig2]

    
if __name__=='__main__':
    
    # import the model defined in root directory
    sys.path.append(str(pathlib.Path(__file__).resolve().parents[2]))
    from model import *

    parser.add_argument("-N",'--nspk_patterns', help="discretization of the grid", type=int, default=1)
    parser.add_argument('--SEEDS', nargs='+', help='various seeds', type=int,
                        default=np.arange(1))
    parser.add_argument("--Pattern_Seed",help="unitless", type=int, default=33)
    parser.add_argument("--Bg_Act_Seed",help="unitless", type=int, default=22)
    parser.add_argument("--Connec_Seed",help="unitless", type=int, default=18)
    
    # input properties: background (arousal variations)
    parser.add_argument("--N_levels",help="unitless", type=int, default=5)
    parser.add_argument("--tswitch",help="ms", type=float, default=2000)
    parser.add_argument("--low_freq_rhythm",help="Hz", type=float, default=2e-3)
    parser.add_argument("--N_repeat",help="unitless", type=int, default=1)
    
    # input properties: presynaptic pattern
    parser.add_argument("--N_aff_stim",help="unitless", type=int, default=100)
    parser.add_argument("--N_source",help="unitless", type=int, default=100)
    parser.add_argument("--N_target",help="unitless", type=int, default=100)
    parser.add_argument("--N_duplicate",help="unitless", type=int, default=10)
    parser.add_argument("--N_event",help="unitless", type=int, default=10)
    parser.add_argument("--t0",help="ms", type=float, default=400)
    parser.add_argument("--time_span",help="ms", type=float, default=500.)
    
    # scan properties
    parser.add_argument("--N_trials",help="unitless", type=int, default=3)
    parser.add_argument("--N_patterns",help="unitless", type=int, default=2)
    
    # recording properties
    parser.add_argument("--N_Vm",help="", type=int, default=4)

    # analysis properties
    parser.add_argument("--N_training",help="unitless", type=int, default=10)
    parser.add_argument("--T_around",help="ms", type=float, default=300.)

    parser.add_argument("--with_neuronpop_shift_synchronous_input", action="store_true")
    parser.add_argument("--with_time_shift_synchronous_input", type=float, default=0.)
    parser.add_argument("--with_neuron_shift_synchronous_input", type=int, default=0)
    parser.add_argument("--show_input", action="store_true")
    # visualization properties
    parser.add_argument('--Trial_Vis', nargs='+', help='various trial visualized', type=int,
                        default=np.arange(2))
    parser.add_argument('--Nnrn_Vis', nargs='+', help='various trial visualized', type=int,
                        default=[])
    parser.add_argument("--Nbar",help="unitless", type=int, default=10)
    
    # filename params
    parser.add_argument('-df', '--data_folder', help='Folder for data', default='sparse_vs_balanced/data/')    
    parser.add_argument("--filename", '-f', help="filename", type=str, default='sparse_vs_balanced/data/various_levels.h5')
    parser.add_argument("--zip_filename", '-zf', help="filename for the zip file", type=str,
                        default='sparse_vs_balanced/data/scan_arousal_levels.zip')

    # execution choice
    parser.add_argument("-a", "--analyze", help="perform analysis of params space", action="store_true")
    parser.add_argument("-p", "--plot", help="plot trial", action="store_true")
    parser.add_argument("-psr", "--plot_spiking_response", help="plot trial", action="store_true")
    parser.add_argument("-rs", "--run_scan", help="RUN SCAN", action="store_true")
    parser.add_argument("-s", "--save", help="save figures", action="store_true")
    
    args = parser.parse_args()
    Model = vars(args)
    Model['p_AffExc_DsInh'] = 0.075
    Model['p_DsInh_RecInh'] = 0.05

    FIGS = None
    if args.analyze:
        FIGS = decoding_analysis(Model['zip_filename'],
                          N_training=Model['N_training'],
                          T_around=Model['T_around'])
    elif args.plot:
        data = load_dict_from_hdf5(Model['filename'])
        fig1 = one_Vm_fig(data, Model)
        fig2 = pop_act_time_course(data, Model)
        FIGS = [fig1, fig2]
    elif args.show_input:
        FIGS = show_input(Model)
    elif args.run_scan:
        run_scan(Model, fix_missing_only=True)
    elif args.plot_spiking_response:
        FIGS = plot_spiking_resp_single_trial(Model)
    else:
        run_sim(Model,
                KEY_NOT_TO_RECORD=[],
                with_pop_act=True,
                verbose=True)

    if (FIGS is not None):
        if not args.save:
            ntwk.show()
        else:
            for i, fig in enumerate(FIGS):
                save_on_desktop(fig, figname=str(i)+'.svg')
    

