import numpy as np
from itertools import product
import sys, pathlib, os
sys.path.append(str(pathlib.Path(__file__).resolve().parents[1]))
import neural_network_dynamics.main as ntwk
from sparse_vs_balanced.running_2pop_model import run_2pop_ntwk_model
from sparse_vs_balanced.running_3pop_model import run_3pop_ntwk_model
from data_analysis.IO.hdf5 import load_dict_from_hdf5
from graphs.my_graph import *
from matplotlib import ticker
# everything stored within a zip file
from scipy.special import erf
from data_analysis.processing.signanalysis import gaussian_smoothing
from graphs.plot_export import put_list_of_figs_to_svg_fig
from matplotlib.cm import copper
import zipfile

Blue, Orange, Green, Red, Purple, Brown, Pink, Grey,\
    Kaki, Cyan = '#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',\
    '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf'

def get_scan(Model, with_png_export=False,
             filename=None):

    if filename is None:
        filename=str(Model['zip_filename'])
    zf = zipfile.ZipFile(filename, mode='r')
    
    data = zf.read(filename.replace('.zip', '_Model.npz'))
    with open(filename.replace('.zip', '_Model.npz'), 'wb') as f: f.write(data)
    Model = dict(np.load(filename.replace('.zip', '_Model.npz')).items())
    
    seeds = Model['SEEDS']
    
    DATA = []
    for j in range(len(seeds)):
        fn = Model['FILENAMES'][j]
        data = zf.read(fn)
        with open(fn, 'wb') as f: f.write(data)
        with open(fn, 'rb') as f: data = load_dict_from_hdf5(fn)
        DATA.append(data)
    return Model, seeds, DATA


def run_sim(Model, SEED=4):

    def set_inh_current(NTWK):
        NTWK['POPS'][1].I0 = Model['Iamp']*ntwk.pA
        # NTWK['POPS'][2].I0 = Model['Iamp']*ntwk.pA
        
    def rm_inh_current(NTWK):
        NTWK['POPS'][1].I0 = 0*ntwk.pA
        # NTWK['POPS'][2].I0 = 0*ntwk.pA
        
    INTERMEDIATE_INSTRUCTIONS = [
        {'time':Model['delay'], 'function':set_inh_current},
        {'time':Model['delay']+Model['duration'], 'function':rm_inh_current}]
    
    NTWK = run_3pop_ntwk_model(Model,
                               with_Vm=10,
                               filename=Model['filename'],
                               INTERMEDIATE_INSTRUCTIONS=INTERMEDIATE_INSTRUCTIONS,
                               tstop=Model['delay']+2.*Model['duration'], SEED=SEED)
    return NTWK

def run_scan(Model):
    
    zf = zipfile.ZipFile(Model['zip_filename'], mode='w')

    seeds = Model['SEEDS']
    Model['FILENAMES'] = np.empty(len(seeds), dtype=object)
    
    for j in range(len(seeds)):
        fn = Model['data_folder']+str(seeds[j])+\
             '_'+str(np.random.randint(100000))+'.h5'
        Model['FILENAMES'][j] = fn
        print('running configuration ', fn)
        run_3pop_ntwk_model(Model,
                            faff_waveform_func=waveform,
                            with_Vm=5,
                            tstop=Model['delay']+2.*Model['duration'], SEED=SEED)
        zf.write(fn)
        
    # writing the parameters
    np.savez(Model['zip_filename'].replace('.zip', '_Model.npz'), **Model)
    zf.write(Model['zip_filename'].replace('.zip', '_Model.npz'))

    zf.close()

def four_Vm_traces(data,
                   Tbar = 200,
                   Vshift=0, vpeak=0, vbottom=-80, nVM=np.arange(5)):

    tzoom=[100, np.inf]
    fig, ax = plt.subplots(1, figsize=(5, 3))
    
    t = np.arange(int(data['tstop']/data['dt']))*data['dt']
    cond = (t>tzoom[0]) & (t<tzoom[1])
    ## excitatory traces:
    for i in nVM[::-1]:
        ax.plot(t[cond], data['VMS_RecExc'][i][cond]+Vshift*i, color=copper(i/3/1.5), lw=.5)
    # adding spikes
    for i in nVM[::-1]:
        tspikes = data['tRASTER_RecExc'][np.argwhere(data['iRASTER_RecExc']==i).flatten()]
        for ts in tspikes[(tspikes>tzoom[0]) & (tspikes<tzoom[1])]:
            ax.plot([ts, ts], Vshift*i+np.array([-50, vpeak]), '-',
                    color=copper(i/3/1.5), lw=.5)
    ax.fill_between([800, 1200],
        ax.get_ylim()[0]*np.ones(2), ax.get_ylim()[1]*np.ones(2), color='y', alpha=.2)
    # # ## inhibitory trace:
    # cond = (t>tzoom[0]) & (t<tzoom[1])
    # ax.plot(t[cond], data['VMS_RecInh'][0][cond]+Vshift*3, color=Red, lw=1)
    # tspikes = data['tRASTER_RecInh'][np.argwhere(data['iRASTER_RecInh']==0).flatten()]
    # for ts in tspikes[(tspikes>tzoom[0]) & (tspikes<tzoom[1])]:
    #     ax.plot([ts, ts], Vshift*3+np.array([-53, vpeak]), '--', color=Red, lw=1)

    ax.plot(tzoom[0]*np.ones(2), [-35, -25], lw=3, color='gray')
    ax.annotate('10mV',(-0.1, .4), rotation=90, fontsize=14, xycoords='axes fraction')
    ax.plot([tzoom[0],tzoom[0]+Tbar], [-75, -75], lw=3, color='gray')
    ax.annotate(str(Tbar)+' ms',
                 (0.1, -0.1), fontsize=14, xycoords='axes fraction')
    set_plot(ax, [], xticks=[], yticks=[])
    return fig

def raster_plot(data, Nmax = 1000, Tbar=100, Nbar=100):
    
    tzoom=[100, np.inf]
    fig, ax = plt.subplots(1, figsize=(5, 3))

    t = np.arange(int(data['tstop']/data['dt']))*data['dt']
    ## excitatory traces:

    ax.plot([t[0], t[-1]], np.zeros(2), 'w.', ms=0.1)
    for i in range(int(data['N_RecExc'][0]))[:Nmax]:
        tspikes = data['tRASTER_RecExc'][np.argwhere(data['iRASTER_RecExc']==i).flatten()]
        tspikes = tspikes[(tspikes>tzoom[0]) & (tspikes<tzoom[1])]
        ax.plot(tspikes, i*np.ones(len(tspikes)), 'o', ms=1,
                color=copper(i/Nmax))
    ax.fill_between([800, 1200],
        ax.get_ylim()[0]*np.ones(2), ax.get_ylim()[1]*np.ones(2), color='y', alpha=.2)

    ax.plot(tzoom[0]*np.ones(2), [0,Nbar], lw=3, color='gray')
    ax.annotate(str(Nbar)+' neurons',(-0.1, .4), rotation=90, fontsize=14, xycoords='axes fraction')
    ax.plot([tzoom[0],tzoom[0]+Tbar], [-75, -75], lw=3, color='gray')
    ax.annotate(str(Tbar)+' ms',
                 (0.1, -0.1), fontsize=14, xycoords='axes fraction')
    set_plot(ax, [], xticks=[], yticks=[])
    return fig

if __name__=='__main__':
    
    # import the model defined in root directory
    sys.path.append(str(pathlib.Path(__file__).resolve().parents[2]))
    from model import *

    # parameters of the input
    
    parser.add_argument("--Iamp",help="Amplitude of the current in pA",\
                        type=float, default=-200.)
    parser.add_argument("--duration",help="Duration of the current step in ms",\
                        type=float, default=400.)
    parser.add_argument("--delay",help="Duration of the current step in ms",\
                        type=float, default=800.)
    
    parser.add_argument('--SEEDS', nargs='+', help='various seeds', type=int,
                        default=np.arange(4))    
    # additional stuff
    parser.add_argument('-df', '--data_folder', help='Folder for data', default='data/')    
    parser.add_argument("--zip_filename", '-f', help="filename for the zip file",type=str,
                        default='data/photoinhibition.zip')
    parser.add_argument("-a", "--analyze", help="perform analysis of params space",
                        action="store_true")
    parser.add_argument("-rm", "--run_multiple_seeds", help="run with multiple seeds",
                        action="store_true")
    parser.add_argument("--debug", help="debug", action="store_true")
    
    args = parser.parse_args()
    Model = vars(args)

    Model['p_AffExc_DsInh'] = 0.075
    Model['p_DsInh_RecInh'] = 0.05
    Model['F_AffExc'] = 4.5

    if args.analyze:
        analyze_sim(Model)
        ntwk.show()
    elif args.run_multiple_seeds:
        Model['filename'] = 'sparse_vs_balanced/data/photoinhibition.h5'
        run_scan(Model)
    elif args.debug:
        data = load_dict_from_hdf5('data/photoinhibition_debug.h5')
        fig = raster_plot(data)
        fig2 = four_Vm_traces(data)
        # fig.savefig('/Users/yzerlaut/Desktop/temp.svg')
        # fig2.savefig('/Users/yzerlaut/Desktop/temp2.svg')
        ntwk.show()
    else:
        Model['filename'] = 'data/photoinhibition_debug.h5'
        NTWK = run_sim(Model)
        Nue, Nui, Nud = NTWK['POP_ACT'][0].rate/ntwk.Hz, NTWK['POP_ACT'][1].rate/ntwk.Hz,\
                        NTWK['POP_ACT'][2].rate/ntwk.Hz
        ntwk.plot(NTWK['POP_ACT'][0].t/ntwk.ms, gaussian_smoothing(Nue,int(20./0.1)))
        ntwk.plot(NTWK['POP_ACT'][0].t/ntwk.ms, gaussian_smoothing(Nui,int(20./0.1)))
        ntwk.plot(NTWK['POP_ACT'][0].t/ntwk.ms, gaussian_smoothing(Nud,int(20./0.1)))
        ntwk.show()

