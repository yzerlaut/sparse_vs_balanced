import numpy as np
import sys, pathlib, os
sep = os.path.sep # Ms-Win vs UNIX
sys.path.append(str(pathlib.Path(__file__).resolve().parents[1]))
# for running simulations
from sparse_vs_balanced.running_2pop_model import run_2pop_ntwk_model
from sparse_vs_balanced.running_3pop_model import run_3pop_ntwk_model
from itertools import product
# for analysis
import neural_network_dynamics.main as ntwk
from data_analysis.IO.hdf5 import load_dict_from_hdf5
from graphs.my_graph import *
from matplotlib import ticker
from PIL import Image # BITMAP (png, jpg, ...)
from fpdf import FPDF
# everything stored within a zip file
import zipfile
Blue, Orange, Green, Red, Purple, Brown, Pink, Grey,\
    Kaki, Cyan = '#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',\
    '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf'

def my_logspace(x1, x2, n):
    return np.logspace(np.log(x1)/np.log(10), np.log(x2)/np.log(10), n)

def get_scan(Model, filename=None):

    if filename is None:
        filename=str(Model['zip_filename'])
    zf = zipfile.ZipFile(filename, mode='r')

    data = zf.read(filename.replace('.zip', '_Model.npz'))

    with open(filename.replace('.zip', '_Model.npz'), 'wb') as f: f.write(data)
    Model = dict(np.load(filename.replace('.zip', '_Model.npz')).items())
    F_aff, seeds = Model['F_AffExc_array'], Model['SEEDS']

    DATA = []
    for i, j in product(range(len(F_aff)), range(len(seeds))):
        
        fn = Model['FILENAMES'][i,j]
        print(fn)
        data = zf.read(fn)
        with open(fn, 'wb') as f: f.write(data)
        with open(fn, 'rb') as f: data = load_dict_from_hdf5(fn)
        data['faff'], data['seed'], data['ifa'], data['iseed'] = F_aff[i], seeds[j], i, j
        DATA.append(data)
        
    return Model, F_aff, seeds, DATA
    
def analyze_scan(Model,
                 irreg_criteria=0.01, synch_criteria=0.9,
                 exc_pop_key='RecExc', inh_pop_key='RecInh',
                 filename=None):

    Model, F_aff, seeds, DATA = get_scan(Model,
                                         filename=filename)
    
    if filename is None:
        filename=str(Model['zip_filename'])

    # printing parameters
    print('=========================================================')
    print('---------- NETWORK AND SIMULATION PARAMETERS -------------')
    print('=========================================================')
    # for i, (key, val) in enumerate(Model.items()):
    #     print(key, val)

    ANALYSIS = {}
    for key in ['faff', 'mean_DsInh',
                'synchrony', 'irregularity', 'balance_'+exc_pop_key,\
                'mean_exc', 'mean_inh', 'meanIe_'+exc_pop_key, 'meanIi_'+exc_pop_key,\
                'meanIe_'+inh_pop_key, 'meanIi_'+inh_pop_key,
                'meanIe_Rec', 'meanIe_Aff', 'meanIi_Rec',\
                'muV_'+exc_pop_key, 'smuV_'+exc_pop_key,\
                'sV_'+exc_pop_key, 'ssV_'+exc_pop_key,\
                'gV_'+exc_pop_key, 'sgV_'+exc_pop_key,\
                'Tv_'+exc_pop_key, 'sTv_'+exc_pop_key,\
                'fr_'+exc_pop_key, 'sfr_'+exc_pop_key]:
        ANALYSIS[key] = np.zeros(np.shape(Model['FILENAMES']))

        
    print('Running analysis [...]')
    for i in range(len(DATA)):
        a, s = DATA[i]['ifa'], DATA[i]['iseed']
        print('file ', i, ', faff ', a, ', seed ', s)
        ANALYSIS['faff'][a,s] = DATA[i]['faff']
        
        output = ntwk.get_all_macro_quant(DATA[i],
                                          exc_pop_key=exc_pop_key,
                                          inh_pop_key=inh_pop_key,
                                          other_pops=['DsInh'])

        for key in ['mean_DsInh', 'synchrony', 'irregularity',\
                    'balance_'+exc_pop_key,\
                    'mean_exc', 'mean_inh', 'meanIe_'+exc_pop_key, 'meanIi_'+exc_pop_key,\
                    'meanIe_'+inh_pop_key,
                    'meanIe_Rec', 'meanIe_Aff', 'meanIi_Rec']:
            ANALYSIS[key][a,s] = output[key]
        ANALYSIS['meanIi_'+inh_pop_key][a,s] = -output['meanIi_'+inh_pop_key] # absolute value !
            
        # Vm analysis and firing analysis of exc cells
        muV, sV, gV, Tv = ntwk.get_Vm_fluct_props(DATA[i], pop=exc_pop_key)
        fr = ntwk.single_cell_firing_rate(DATA[i], pop=exc_pop_key)
        for key, val in zip(['muV_'+exc_pop_key, 'sV_'+exc_pop_key, 'gV_'+exc_pop_key, 'Tv_'+exc_pop_key, 'fr_'+exc_pop_key],
                            [muV, sV, gV, Tv, fr]):
            if len(val)>0:
                ANALYSIS[key][a,s], ANALYSIS['s'+key][a,s] = np.mean(val), np.std(val)
            else:
                ANALYSIS[key][a,s], ANALYSIS['s'+key][a,s] = np.inf, np.inf
        
    print('Done with analysis, now saving [...]')
    np.savez(filename.replace('.zip', '_analyzed.npz'), **ANALYSIS)
    
def run_scan(Model):
    
    zf = zipfile.ZipFile(Model['zip_filename'], mode='w')

    #####################################################
    ############# GRID OF PARAMETER SPACE ###############
    #####################################################

    F_aff, seeds = Model['F_AffExc_array'], Model['SEEDS']
    
    # Model['FILENAMES'] = np.empty((len(F_aff), len(seeds)), dtype=str)
    Model['FILENAMES'] = np.empty((len(F_aff), len(seeds)), dtype='<U53')
    
    for i, j in product(range(len(F_aff)), range(len(seeds))):
        fn = Model['data_folder']+str(F_aff[i])+'_'+str(seeds[j])+\
             '_'+str(np.random.randint(100000))+'.h5'
        Model['FILENAMES'][i,j] = str(fn)
        print('running configuration ', fn)
        Model['F_AffExc'] = F_aff[i]
        if Model['p_AffExc_DsInh']>0:
            run_3pop_ntwk_model(Model, filename=fn, SEED=seeds[j], with_Vm=Model['N_rec_Vm'])
        else:
            run_2pop_ntwk_model(Model, filename=fn, SEED=seeds[j], with_Vm=Model['N_rec_Vm'])
        zf.write(fn)
    
    # writing the parameters
    np.savez(Model['zip_filename'].replace('.zip', '_Model.npz'), **Model)
    zf.write(Model['zip_filename'].replace('.zip', '_Model.npz'))

    zf.close()

    
if __name__=='__main__':

    # import the model defined in root directory
    sys.path.append(str(pathlib.Path(__file__).resolve().parents[2]))
    from model import *

    parser.add_argument('--F_AffExc_array', nargs='+', help='Afferent firing rates', type=float,
                        default=my_logspace(1, 25, 2))    
    parser.add_argument('--SEEDS', nargs='+', help='various seeds', type=int,
                        default=np.arange(2))    
    parser.add_argument('--N_rec_Vm', type=int, default=4)    
    parser.add_argument('--N_SEED', type=int, default=1) # USELESS
    parser.add_argument('-df', '--data_folder', help='Folder for data', default='data'+sep)    
    parser.add_argument("-a", "--analyze", help="ANALYSIS", action="store_true")

    
    # additional stuff
    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
    parser.add_argument("--zip_filename", '-f', help="filename for the zip file",type=str, default='data.zip')

    args = parser.parse_args()
    Model = vars(args) # overwrite model based on passed arguments
    
    if args.analyze:
        analyze_scan(Model)
    else:
        run_scan(Model)
    
