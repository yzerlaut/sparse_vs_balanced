python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_dsnh_aff_coupling_0.0.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_dsnh_aff_coupling_0.02.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_dsnh_aff_coupling_0.05.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_dsnh_aff_coupling_0.07.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_dsnh_aff_coupling_0.1.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_nfactor_0.5.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_nfactor_1.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_nfactor_2.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_nfactor_4.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_nfactor_8.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_n_pconn_factor_0.5.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_n_pconn_factor_1.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_n_pconn_factor_2.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_n_pconn_factor_4.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_n_pconn_factor_8.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_Qfactor_0.2.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_Qfactor_0.5.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_Qfactor_1.0.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_Qfactor_2.0.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_Qfactor_5.0.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_QaValues_2.0.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_QaValues_3.0.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_QaValues_4.0.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_QaValues_5.0.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_QeValues_0.5.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_QeValues_1.0.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_QeValues_2.0.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_QeValues_4.0.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_QeValues_6.0.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_QiValues_5.0.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_QiValues_10.0.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_QiValues_15.0.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_QiValues_20.0.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_QiValues_30.0.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_QiValues_40.0.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_vthreinh_-57.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_vthreinh_-53.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_vthreinh_-51.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_vthreinh_-49.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_pconn_0.02.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_pconn_0.05.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_pconn_0.07.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_pconn_0.1.zip & 
python sparse_vs_balanced/varying_AffExc.py -a -f sparse_vs_balanced/data/dAE_pconn_0.15.zip & 
