import sys, pathlib
sys.path.append(str(pathlib.Path(__file__).resolve().parents[1]))
import neural_network_dynamics.main as ntwk
import numpy as np

def run_3pop_ntwk_model(Model,
                        tstop=2000.,
                        filename='data/sas.h5',
                        with_raster=True,
                        with_Vm=4,
                        with_pop_act=True,
                        with_synaptic_currents=True,
                        with_synaptic_conductances=True,
                        faff_waveform_func=None,
                        additional_spikes_func=None,
                        with_synchronous_input=False,
                        with_time_shift_synchronous_input=0.,
                        with_neuron_shift_synchronous_input=0,
                        with_neuronpop_shift_synchronous_input=False,
                        INTERMEDIATE_INSTRUCTIONS=[],
                        KEY_NOT_TO_RECORD=[],
                        verbose=False,
                        SEED=2,
                        BG_ACT_SEED=3,
                        CONNEC_SEED=18):

    Model['SEED'], Model['tstop'] = BG_ACT_SEED, tstop # adjusting simulation seed and length 
    
    NTWK = ntwk.build_populations(Model, ['RecExc', 'RecInh', 'DsInh'],
                                  AFFERENT_POPULATIONS=['AffExc'],
                                  with_raster=with_raster,
                                  with_Vm=with_Vm,
                                  with_pop_act=with_pop_act,
                                  with_synaptic_currents=with_synaptic_currents,
                                  with_synaptic_conductances=with_synaptic_conductances,
                                  verbose=verbose)

    ntwk.build_up_recurrent_connections(NTWK, SEED=CONNEC_SEED, verbose=verbose)

    #######################################
    ########### AFFERENT INPUTS ###########
    #######################################

    
    t_array = ntwk.arange(int(Model['tstop']/Model['dt']))*Model['dt']

    if faff_waveform_func is None:
       faff_waveform =  Model['F_AffExc']+0.*t_array
    else:
       faff_waveform = faff_waveform_func(t_array, Model)
       
    # # # afferent excitation onto cortical excitation and inhibition
    for i, tpop in enumerate(['RecExc', 'RecInh', 'DsInh']): # on excitation, inhibition and disinhibition

        Model['M_AffExc_'+tpop] = ntwk.build_aff_to_pop_matrix('AffExc', tpop, Model, SEED=CONNEC_SEED+i)
        
        if additional_spikes_func is not None:
            additional_spikes = additional_spikes_func('AffExc', tpop, Model)
        else:
            additional_spikes={'indices':[], 'times':[]}

        ntwk.construct_feedforward_input(NTWK, tpop, 'AffExc',
                                         t_array, faff_waveform,
                                         additional_spikes=additional_spikes,
                                         verbose=verbose,
                                         SEED=int(37*BG_ACT_SEED+i)%13)
    
    ################################################################
    ## --------------- Initial Condition ------------------------ ##
    ################################################################
    ntwk.initialize_to_rest(NTWK)
    
    #####################
    ## ----- Run ----- ##
    #####################
    network_sim = ntwk.collect_and_run(NTWK,
                                       verbose=verbose,
                                       INTERMEDIATE_INSTRUCTIONS=INTERMEDIATE_INSTRUCTIONS)

    if filename!='':
        ntwk.write_as_hdf5(NTWK, filename=filename, KEY_NOT_TO_RECORD=KEY_NOT_TO_RECORD)
        print('sim output written as:', filename)
    else:
        print('sim output not saved as hdf5')

    return NTWK

if __name__=='__main__':

    from data_analysis.processing.signanalysis import gaussian_smoothing
    # import the model defined in root directory
    sys.path.append(str(pathlib.Path(__file__).resolve().parents[2]))
    from model import *

    args = parser.parse_args()
    Model = vars(args)
    Model['p_AffExc_DsInh'] = 0.075
    Model['p_DsInh_RecInh'] = 0.05

    def additional_spikes_func(pre_pop, target_pop, Model):
        if (target_pop=='RecExc') & (pre_pop=='AffExc'):
            pre_indices = np.arange(int(Model['N_'+pre_pop]))
            pre_times = np.ones(int(Model['N_'+pre_pop]))*30.
            additional_spikes={}
            additional_spikes['indices'],\
                additional_spikes['times'] = \
                ntwk.translate_aff_spikes_into_syn_target_events(pre_indices, pre_times,
                                                                 Model['M_'+pre_pop+'_'+target_pop])
        else:
            additional_spikes={'indices':[], 'times':[]},
        return additional_spikes
    
    if Model['p_AffExc_DsInh']==0.:
        print('---------------------------------------------------------------------------')
        print('to run the 3 pop model, you need to set an afferent connectivity proba !')
        print('e.g run: \n                python running_3pop_model.py --p_AffExc_DsInh 0.1')
        print('---------------------------------------------------------------------------')
    else:
        NTWK = run_3pop_ntwk_model(Model,
                                   tstop=args.tstop,
                                   additional_spikes_func=additional_spikes_func,
                                   verbose=True)
        Nue = NTWK['POP_ACT'][0].rate/ntwk.Hz
        ntwk.plot(NTWK['POP_ACT'][0].t/ntwk.ms, gaussian_smoothing(Nue,int(20./0.1)))
        ntwk.show()
