import numpy as np
import sys, pathlib, os
sep = os.path.sep # Ms-Win vs UNIX
sys.path.append(str(pathlib.Path(__file__).resolve().parents[1]))
from model import pass_arguments_of_new_model, Model
# to plot parameter variations, we use a color code:
from graphs.my_graph import build_bar_legend
import matplotlib.pylab as plt
from matplotlib.cm import viridis
from sparse_vs_balanced.varying_AffExc import get_scan
from sparse_vs_balanced.plot_state_signature import raster_plot, four_Vm_traces, one_Isyn_sample


#############################################################
######                    BASH RUN                     ######
#############################################################


def my_logspace(x1, x2, n):
    return np.logspace(np.log(x1)/np.log(10), np.log(x2)/np.log(10), n)

def run_one_config_of_dep_on_ExcAff(Model_modif,
                                    filename='data.zip',
                                    Faff=my_logspace(3., 25, 30),
                                    seeds=np.arange(1, 5),
                                    dt=0.1, tstop=10000,\
                                    Sim_string ='',
                                    Analysis_string = '',
                                    Fetch_data_string = '',
                                    Fetch_analysis_string = ''):

    for s in [Sim_string, Analysis_string, Fetch_data_string, Fetch_analysis_string]:
        s += 'echo --------------------------------------------------------------------'
        s += 'echo -------'+filename+'------------------------------------'
    
    Model2 = Model.copy()
    Model2['dt'], Model2['tstop'] = dt, tstop
    for key, val in Model_modif.items():
        Model2[key] = val
    tf_file = 'sparse_vs_balanced/varying_AffExc.py'

    Sim_string += 'python '+tf_file+pass_arguments_of_new_model(Model2)
    Sim_string += ' --F_AffExc_array'
    for fa in Faff: Sim_string += ' '+str(fa)
    Sim_string += ' --SEEDS'
    for s in seeds: Sim_string += ' '+str(s)
    Sim_string += ' -df sparse_vs_balanced/data'+os.path.sep
    Sim_string += ' -f sparse_vs_balanced/data'+os.path.sep+filename+' & \n'

    # for the analysis, not possible to do it in parrallel for memory problems...
    Analysis_string += 'python '+tf_file+' -a -f sparse_vs_balanced/data'+os.path.sep+filename+' \n'

    Fetch_data_string += 'sget sparse_vs_balanced/data'+os.path.sep+filename+' \n'
    Fetch_analysis_string += 'sget sparse_vs_balanced/data/'+filename.replace('.zip', '_analyzed.npy')+' \n'

    return Sim_string, Analysis_string, Fetch_data_string, Fetch_analysis_string

def write_bash_scripts(Sim_string, Analysis_string,
                       Fetch_data_string, Fetch_analysis_string, FILENAME):
    bash_file = open(FILENAME, 'w')
    bash_file.write(Sim_string);bash_file.close()
    bash_file = open(FILENAME.replace('bash_scan/', 'bash_scan/analysis_'), 'w')
    bash_file.write(Analysis_string);bash_file.close()
    bash_file = open(FILENAME.replace('bash_scan/', 'bash_scan/fetch_data_'), 'w')
    bash_file.write(Fetch_data_string);bash_file.close()
    bash_file = open(FILENAME.replace('bash_scan/', 'bash_scan/fetch_analysis_'), 'w')
    bash_file.write(Fetch_analysis_string);bash_file.close()


#############################################################
########                    PLOT                     ######## 
#############################################################


from graphs.my_graph import set_plot

def plot_effect_of_params(PARAMS, base_filename, label,
                          xticks=[5,10,20], xticks_labels=['5','10','20'],
                          yticks=[0.01, 0.1, 1., 10.], yticks_labels=['<0.01', '0.1', '1', '10'],\
                          SI_synch = 0.03, SR_synch=0.96):
    fig, AX = plt.subplots(1, 4, figsize=(13,2));
    plt.subplots_adjust(wspace=1.1, right=.87)
    
    for ax in AX: ax.set_xscale('log')
    for ax in [AX[0], AX[1], AX[3]]: ax.set_yscale('log');
    
    for i, p in enumerate(PARAMS):
        DATA = np.load(base_filename+str(p)+'_analyzed.npy')
        Fa = DATA[0]
        # getting seeds and unique Fa values
        F_aff = np.unique(Fa)
        seeds = np.arange(int(len(Fa)/len(F_aff)))
        Synch, Ii_over_Ie, NUe, NUi = DATA[1].reshape((len(F_aff), len(seeds))), DATA[3].reshape((len(F_aff), len(seeds))),\
            DATA[4].reshape((len(F_aff), len(seeds))), DATA[5].reshape((len(F_aff), len(seeds)))
        Fe, sFe, Fi, sFi = NUe.mean(axis=1), NUe.std(axis=1), NUi.mean(axis=1), NUi.std(axis=1)
        # to limit data range, hence the <0.01 legend:
        Fe[Fe<0.01], sFe[Fe<0.01], Fi[Fi<0.01], sFi[Fi<0.01] = 0.01, 0., 0.01, 0. 
        sFe[Fe-sFe<1e-2], sFi[Fi-sFi<1e-2] = np.abs(Fe[Fe-sFe<1e-2]-1e-2), np.abs(Fi[Fi-sFi<1e-2]-1e-2)
        Synch, sSynch = np.abs(Synch).mean(axis=1), np.abs(Synch).std(axis=1)
        Ii_over_Ie, sIi_over_Ie = np.abs(Ii_over_Ie).mean(axis=1), np.abs(Ii_over_Ie).std(axis=1)
        Synch[Synch<1e-4] = 1.1e-4
        sSynch[Synch-sSynch<1e-4] = np.abs(Synch[Synch-sSynch<1e-4]-1e-4)
        col = viridis(i/(len(PARAMS)-1))
        for ax, y, sy, fmt in zip(AX, [Fe, Fi, Ii_over_Ie, Synch], [sFe, sFi, sIi_over_Ie, sSynch],
                                  ['o-', 'o-', 'o-', 'o']):
            ax.errorbar(F_aff, y, yerr=sy, fmt=fmt, ms=3, color=col, lw=2)

    for ax in AX: ax.set_xlim([F_aff[0], F_aff[-1]])

    set_plot(AX[0], xlabel='$\\nu_a$ (Hz)', ylabel='$\\nu_e$ (Hz)',
             xticks=xticks, xticks_labels=xticks_labels,
             yticks=yticks, yticks_labels=yticks_labels)
    set_plot(AX[1], xlabel='$\\nu_a$ (Hz)', ylabel='$\\nu_i$ (Hz)',
             xticks=xticks, xticks_labels=xticks_labels,
             yticks=yticks, yticks_labels=yticks_labels)
    set_plot(AX[2], xlabel='$\\nu_a$ (Hz)', ylabel='$ \| I_i$ / $I_e \|$',
             xticks=xticks, xticks_labels=xticks_labels, yticks=[0., 0.5, 1.])
    AX[3].plot(F_aff, SR_synch+0.*F_aff, 'k--', label='SR act.')
    AX[3].plot(F_aff, SI_synch+0.*F_aff, 'k:', label='SI act.')
    AX[3].plot([Fa[0], Fa[0]], [1e-4, 1.], 'w.', ms=0.01)
    set_plot(AX[3], ylabel='synch. index', xlabel='$\\nu_a$ (Hz)',
             xticks=xticks, xticks_labels=xticks_labels,
             yticks=[1e-4, 1e-3, 0.01, 0.1, 1.],
             yticks_labels=['<1e-4', '1e-3', '1e-2', '1e-1', '1'])
    # let's build up the color legend for this parameter
    ax = fig.add_axes([0.91, 0.01, 0.02, 0.8])
    build_bar_legend(np.arange(len(PARAMS)+1), ax, viridis, label=label,\
                     ticks_labels= [str(p) for p in PARAMS]);
    return fig


############################################################################
##### visualizing the dynamics of a specific set of parameters #############
# RASTER PLOT + 4 VMS SAMPLES (3 Exc + 1 Inh) + 1 SYNAPTIC CURRENTS SAMPLE #
############################################################################

def look_at_specific_realisation(PARAMS, N, base_filename,
                                 Faff=5., label='$Q_a$', unit='',
                                 pop='RecExc',
                                 tzoom=[1000, 2000], Nmax=1000, Tbar=50, Nbar=200):
    from model import Model
    Model, FA, seeds, DATA = get_scan(Model, filename=base_filename+str(PARAMS[N])+'.zip')
    data = DATA[np.argmin(np.abs(FA-Faff))]
    true_faff = FA[np.argmin(np.abs(FA-Faff))]
    fig, AX = plt.subplots(1, 3, figsize=(8,2.5));plt.subplots_adjust(wspace=.2)
    fig.suptitle('$\\nu_a$='+str(round(true_faff,1))+'Hz, '+label+'='+str(PARAMS[N])+unit)
    raster_plot(data, AX[0], tzoom=tzoom, Nmax=Nmax, Tbar=Tbar, Nbar=Nbar)
    four_Vm_traces(data, AX[1], Tbar=Tbar, tzoom=tzoom)
    one_Isyn_sample(data, AX[2], Tbar=Tbar, tzoom=tzoom, pop_key=pop)
