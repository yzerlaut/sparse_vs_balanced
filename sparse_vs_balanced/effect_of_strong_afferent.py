import numpy as np
from itertools import product
import sys, pathlib, os
sys.path.append(str(pathlib.Path(__file__).resolve().parents[1]))
import neural_network_dynamics.main as ntwk
from sparse_vs_balanced.running_2pop_model import run_2pop_ntwk_model
from sparse_vs_balanced.running_3pop_model import run_3pop_ntwk_model
from graphs.my_graph import *
from matplotlib import ticker
# everything stored within a zip file
from scipy.special import erf
from data_analysis.processing.signanalysis import gaussian_smoothing, autocorrel
from graphs.plot_export import put_list_of_figs_to_svg_fig
from matplotlib.cm import copper
import zipfile

Blue, Orange, Green, Red, Purple, Brown, Pink, Grey,\
    Kaki, Cyan = '#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',\
    '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf'
Inter = get_linear_colormap(Blue, Orange)(.5) # for the intermediate regime

def waveform(t, Model):
    waveform = 0*t
    # first waveform
    for tt, fa in zip(\
         2.*Model['rise']+np.arange(3)*(3.*Model['rise']+Model['DT']),
                      [Model['Faff1'], Model['Faff2'], Model['Faff3']]):
        waveform += fa*\
             (1+erf((t-tt)/Model['rise']))*\
             (1+erf(-(t-tt-Model['DT'])/Model['rise']))/4
    return waveform

def aff_pop_time_course(Model, t0=400.,
                        FIGSIZE=(8,2),
                        YTICKS=[0, 5, 10], XBAR=500):
    
    tstop = Model['rise']+3*(3.*Model['rise']+Model['DT'])
    t = np.arange(int(tstop/Model['dt']))*Model['dt']
    faff = waveform(t, Model)
    cond = (t>t0)
    
    fig3, ax = plt.subplots(1, figsize=FIGSIZE)
    plt.subplots_adjust(bottom=0.3)
    plt.plot(t[cond], faff[cond], 'k-')
    plt.plot([100, 100+XBAR], [2., 2.], color='gray', lw=3)
    plt.annotate(str(int(XBAR))+'ms', (150, 2.5))
    for tt, color in zip(\
                         2.*Model['rise']+np.arange(3)*(3.*Model['rise']+Model['DT']),
                         [Blue, Orange, Inter]):
        print(tt)
        plt.plot([tt+2.*Model['rise'], tt+Model['DT']-2.*Model['rise']], Model['Faff2']/2.*np.ones(2),
                 color=color, lw=5)
    set_plot(ax, ['left'], ylabel='$\\nu_a$ (Hz)',
             yticks=YTICKS, xticks=[], xlim=[t0, tstop])
    return fig3

def get_scan(Model, with_png_export=False,
             filename=None):

    if filename is None:
        filename=str(Model['zip_filename'])
    zf = zipfile.ZipFile(filename, mode='r')
    
    data = zf.read(filename.replace('.zip', '_Model.npz'))
    with open(filename.replace('.zip', '_Model.npz'), 'wb') as f: f.write(data)
    Model = dict(np.load(filename.replace('.zip', '_Model.npz')).items())
    
    seeds = Model['SEEDS']
    
    DATA = []
    for j in range(len(seeds)):
        fn = Model['FILENAMES'][j]
        data = zf.read(fn)
        with open(fn, 'wb') as f: f.write(data)
        with open(fn, 'rb') as f: data = ntwk.load_dict_from_hdf5(fn)
        DATA.append(data)
    return Model, seeds, DATA

def one_Vm_fig(data, Model,
               iNVm_Exc=np.arange(3), iNVm_Inh=0, iNVm_DsInh=0,
               dV=30, vpeak=-43, smoothing=10e-3, Gl=10,
               FIGSIZE=(6,4), XTICKS=[0, 500, 1000],
               t0=0., tstart=1100., tdur=300, blank_spike=6.):


    tstop = Model['rise']+3*(3.*Model['rise']+Model['DT'])
    t = np.arange(int(tstop/Model['dt']))*Model['dt']
    
    fig1, ax = plt.subplots(1, figsize=FIGSIZE)
    plt.subplots_adjust(top=.99, bottom=.2, left=.01, right=.99)
    # exc
    for i, k in enumerate(iNVm_Exc):
        cond = (t>t0)
        tspikes = data['tRASTER_RecExc'][np.argwhere(data['iRASTER_RecExc']==k).flatten()]
        for ts in tspikes[tspikes>t0]:
            ax.plot([ts, ts],
                    [data['RecExc_Vthre']+i*dV, vpeak+i*dV], '--', lw=1, color=Green)
            cond = cond & np.invert((t>=ts-2.*Model['dt']) & (t<=(ts+blank_spike)))
            ispike = int(ts/data['dt'])
            # data['VMS_RecExc'][k][cond][ispike+1:ispike+int(blank_spike/data['dt'])] = np.inf
        ax.plot(t[cond], data['VMS_RecExc'][k][cond]+i*dV, '-', lw=1, color=Green)

    # inh
    tspikes = data['tRASTER_RecInh'][np.argwhere(data['iRASTER_RecInh']==iNVm_Inh).flatten()]
    cond = (t>t0)
    for ts in tspikes[tspikes>t0]:
        ax.plot([ts, ts], [data['RecInh_Vthre']-dV, vpeak-dV], '--', color=Red, lw=1)
        cond = cond & np.invert((t>=ts-2.*Model['dt']) & (t<=(ts+blank_spike)))
    ax.plot(t[cond], data['VMS_RecInh'][iNVm_Inh][cond]-dV, '-', lw=1, color=Red)
    # dsinh
    cond = (t>t0)
    tspikes = data['tRASTER_DsInh'][np.argwhere(data['iRASTER_DsInh']==iNVm_DsInh).flatten()]
    for ts in tspikes[tspikes>t0]:
        ax.plot([ts, ts],
                [data['DsInh_Vthre']-2*dV, vpeak-2*dV], '--', color=Purple, lw=1)
        cond = cond & np.invert((t>=ts-2.*Model['dt']) & (t<=(ts+blank_spike)))
    ax.plot(t[cond], data['VMS_DsInh'][iNVm_DsInh][cond]-2*dV, '-', lw=1, color=Purple)
    ax.plot([0,0], [-70, -60], color='gray', lw=5)
    ax.annotate('10 mV', (0, -55))
    set_plot(ax, [],
             yticks=[], xticks=[],
             xlim=[t0, tstop])
    
    return fig1

def conductance_time_course(DATA, Model,
                            t0=0., t1=4000.,
                            smoothing=10,
                            FIGSIZE=(2,2),
                            YTICKS=[0, 4, 8],
                            XBAR=300, plot_only=False):
    
    fig2, ax = plt.subplots(1, figsize=FIGSIZE)
    plt.subplots_adjust(left=.3, bottom=.4)
    if plot_only:
        t, MEAN_GE, STD_GE, MEAN_GI, STD_GI = np.load('sparse_vs_balanced/data/analyzed_conductances.npy')
        
        for t0, t1, col, ii in zip([200, 1250, 2300], [900, 1950, 3000], [Blue, Orange, Inter], range(3)):
            cond = (t>t0) & (t<t1)
            mean_ge = np.array([mge[cond].mean() for mge in MEAN_GE])
            mean_gi = np.array([mgi[cond].mean() for mgi in MEAN_GI])
            ax.bar([ii], np.mean(mean_gi/mean_ge), yerr=np.std(mean_gi/mean_ge), color=col, width=0.6)
            
        # ax.plot(t[cond], MEAN_GI.mean(axis=0)[cond]/MEAN_GE.mean(axis=0)[cond], 'k-')
            
        # for m, std, col in zip([MEAN_GE.mean(axis=0), MEAN_GI.mean(axis=0)],
        #                        [STD_GE.mean(axis=0), STD_GI.mean(axis=0)], [Green, Red]):
        #     m[m<1e-2] = 1e-2
        #     std[(m-std)<1e-2] = 0
        #     ax.plot(t[cond], m[cond], color=col, lw=2)
        #     ax.fill_between(t[cond], m[cond]-std[cond], m[cond]+std[cond],
        #                     alpha=.6, color=col, lw=0)
        # ax.set_yscale('log')
        # ax.plot(t[cond], MEAN_GE.mean(axis=0)[cond], color=Green, lw=2, label=r'$G_{e}$')
        # ax.plot(t[cond], MEAN_GI.mean(axis=0)[cond], color=Red, lw=2, label=r'$G_{i}$')
        # ax.fill_between(t[cond],
        #                 MEAN_GE.mean(axis=0)[cond]-STD_GE.mean(axis=0)[cond],
        #                 MEAN_GE.mean(axis=0)[cond]+STD_GE.mean(axis=0)[cond],
        #                 alpha=.6, color=Green)
        # ax.fill_between(t[cond],
        #                 MEAN_GI.mean(axis=0)[cond]-STD_GI.mean(axis=0)[cond],
        #                 MEAN_GI.mean(axis=0)[cond]+STD_GI.mean(axis=0)[cond],
        #                 alpha=.6, color=Red)
        # ax.plot([t0, t0+XBAR], [2., 2.], color='gray', lw=3)
        # ax.annotate(str(int(XBAR))+'ms', (t0, 2.5))
        # ax.legend(frameon=False)

        # for tt, color in zip(\
        #                  2.*Model['rise']+np.arange(3)*(3.*Model['rise']+Model['DT']),
        #                  [Blue, Orange, Inter]):
        #     tt0 = max([t0, tt+2.*Model['rise']])
        #     tt1 = min([t1, tt+Model['DT']-2.*Model['rise']])
        #     ax.plot([tt0, tt1], np.ones(2), color=color, lw=5)

        set_plot(ax, yticks=[0,1,2,3],
                 xticks=[0,1,2], xticks_labels=['$T_1$', '$T_2$', '$T_3$'],
                 ylabel='$G_{i}$/$G_e$')

    else:
        tstop = Model['rise']+3*(3.*Model['rise']+Model['DT'])
        t = np.arange(int(tstop/Model['dt']))*Model['dt']
        ismooth = int(smoothing/(t[1]-t[0]))
        
        MEAN_GE, STD_GE = np.zeros((len(DATA), len(t))), np.zeros((len(DATA), len(t)))
        MEAN_GI, STD_GI = np.zeros((len(DATA), len(t))), np.zeros((len(DATA), len(t)))

        for tt in 2.*Model['rise']+np.arange(3)*(3.*Model['rise']+Model['DT']):
            ## Vm during specific window
            print(tt+2.*Model['rise'], tt+Model['DT']-2.*Model['rise'])
            
        for n in range(len(DATA)):

            data = DATA[n].copy()
            # smoothing conductances
            for i, gsyn in enumerate(data['GSYNe_RecExc']):
                data['GSYNe_RecExc'][i] = gaussian_smoothing(gsyn, ismooth)
            for i, gsyn in enumerate(data['GSYNi_RecExc']):
                data['GSYNi_RecExc'][i] = gaussian_smoothing(gsyn, ismooth)

            MEAN_GE[n, :] = np.array(data['GSYNe_RecExc']).mean(axis=0)/Model['RecExc_Gl']
            STD_GE[n, :] = np.array(data['GSYNe_RecExc']).std(axis=0)/Model['RecExc_Gl']
            MEAN_GI[n, :] = np.array(data['GSYNi_RecExc']).mean(axis=0)/Model['RecExc_Gl']
            STD_GI[n, :] = np.array(data['GSYNi_RecExc']).std(axis=0)/Model['RecExc_Gl']

        np.save('sparse_vs_balanced/data/analyzed_conductances.npy',
                [t, MEAN_GE, STD_GE, MEAN_GI, STD_GI])

    
    return fig2

def pop_act_time_course(DATA, Model, t0=0., smoothing=10,
                        FIGSIZE=(4,2), lower_lim=0.01,
                        YTICKS=[0.01, 1., 100.],
                        YTICKS_LABELS=['<0.01', '1', '100']):
    
    tstop = Model['rise']+3*(3.*Model['rise']+Model['DT'])
    t = np.arange(int(tstop/Model['dt']))*Model['dt']
    ismooth = int(smoothing/Model['dt'])
    MEAN_FE, MEAN_FI, MEAN_FD = [np.zeros((len(DATA), len(t))) for i in range(3)]
    
    for n, data in enumerate(DATA):
        
        MEAN_FE[n, :] = gaussian_smoothing(data['POP_ACT_RecExc'], ismooth)
        MEAN_FI[n, :] = gaussian_smoothing(data['POP_ACT_RecInh'], ismooth)
        MEAN_FD[n, :] = gaussian_smoothing(data['POP_ACT_DsInh'], ismooth)

    faff = waveform(t, Model)
    cond = (t>t0)
    
    fig3, ax = plt.subplots(1, figsize=FIGSIZE)

    for vec, label, color in zip([MEAN_FI, MEAN_FE, MEAN_FD],
                                 ['$\\nu_i$', '$\\nu_e$', '$\\nu_d$'],
                                 [Red, Green, Purple]):
        y = vec.mean(axis=0)
        sy = vec.std(axis=0)
        minus_y, plus_y = y-sy, y+sy
        y[y<lower_lim] = lower_lim
        minus_y[minus_y<lower_lim] = lower_lim
        plus_y[plus_y<lower_lim] = lower_lim
        plt.plot(t[cond], y[cond], color=color, label=label, lw=2)
        plt.fill_between(t[cond], minus_y[cond], plus_y[cond],
                         color=color, alpha=.5, lw=0)

    ax.set_yscale('log')
    ax.legend(frameon=False)
    set_plot(ax, ['left'],
             yticks=YTICKS, yticks_labels=YTICKS_LABELS, 
             ylabel='rate (Hz)', xticks=[], xlim=[t0, tstop])
    return fig3

def hist_of_Vm(DATA, Model,
               tspkdiscard=10.,
               bins=np.linspace(-73, -50, 30),
               FIGSIZE=(2, 2),
               XTICKS=[-70, -60, -50],
               autocorrel_window=100.,
               plot_only=False):
    
    fig4, ax = plt.subplots(1, figsize=FIGSIZE)
    plt.subplots_adjust(bottom=.4)
    fig5, ax2 = plt.subplots(1, figsize=(1.3,1.8))
    plt.subplots_adjust(bottom=.4)

    
    if plot_only:
        HISTS, bins = np.load('sparse_vs_balanced/data/analyzed_Vm_hist.npy')
        for hist, color, lab in zip(\
                                    HISTS,
                                    [Blue, Orange, Inter],
                                    ['$T_1$', '$T_2$', '$T_3$']):
            ax.plot(.5*(bins[1:]+bins[:-1]), hist.mean(axis=0), color=color, lw=2, label=lab)
            ax.fill_between(.5*(bins[1:]+bins[:-1]),
                            hist.mean(axis=0)+hist.std(axis=0),
                            hist.mean(axis=0)-hist.std(axis=0),
                            color=color, lw=0, alpha=.7)
        ax.legend(loc=(1., .2), frameon=False)
        set_plot(ax, xlabel='$V_m$ (mV)', xticks=XTICKS, yticks=[], ylabel='n. count')

        AUTOCORRELS, shift, COUNTS = np.load('sparse_vs_balanced/data/analyzed_autocorrel.npy')
        dt = 0.1
        x = np.arange(int(autocorrel_window/dt))*dt
        for acf, count, color, lab in zip(AUTOCORRELS[::-1], COUNTS[::-1],
                                   [Blue, Orange, Inter][::-1],
                                   ['$T_1$', '$T_2$', '$T_3$'][::-1]):
            ax2.plot(x[x<40],
                     np.mean(acf.T/count, axis=1)[x<40], color=color, lw=2)
            ax2.fill_between(x[x<40],
                             np.mean(acf.T/count, axis=1)[x<40]+np.std(acf.T/count, axis=1)[x<40],
                             np.mean(acf.T/count, axis=1)[x<40]-np.std(acf.T/count, axis=1)[x<40],
                             color=color, lw=0, alpha=.7)
        set_plot(ax2, xlabel='shift (ms)', xticks=[0,20,40], xlim=[0,40], yticks=[0,1], ylabel='n. ACF')
        
    else:
        tstop = Model['rise']+3*(3.*Model['rise']+Model['DT'])
        t = np.arange(int(tstop/Model['dt']))*Model['dt']
        iwindow = int(autocorrel_window/Model['dt'])

        HISTS, AUTOCORRELS, COUNTS = [], [], []
        for tt, color, lab in zip(\
                             2.*Model['rise']+np.arange(3)*(3.*Model['rise']+Model['DT']),
                                  [Blue, Orange, Inter], ['$T_1$', '$T_2$', '$T_3$']):
            ## Vm during specific window
            cond = (t>tt+2.*Model['rise']) & (t<tt+Model['DT']-2.*Model['rise'])
            Vm = np.empty(0)

            HIST, AUTOCORREL, COUNT = [], [], []
            for data in DATA:
                AUTOCORREL.append(np.zeros(iwindow))
                COUNT.append(0)
                # for key in ['RecExc', 'RecInh']:
                for key in ['RecExc']:
                    for i in range(len(data['VMS_'+key])):
                        # then removing spikes
                        tspikes = data['tRASTER_'+key][np.argwhere(data['iRASTER_'+key]==i).flatten()]
                        for ts in tspikes:
                            cond2 = cond &\
                                    np.invert((t>=ts-2.*Model['dt']) & (t<=(ts+tspkdiscard))) &\
                                    (data['VMS_'+key][i]!=Model[key+'_El'])
                        Vm = np.concatenate([Vm, data['VMS_'+key][i][cond2]])
                        # looking at Vm samples in between spikes
                        for ts1, ts2 in zip(np.concatenate([[0],tspikes]), np.concatenate([tspikes, [10000]])):
                            cond2 = cond & np.invert((t>=ts1+tspkdiscard) & (t<=(ts2)))
                            acf, shift = autocorrel(data['VMS_'+key][i][cond2], autocorrel_window, Model['dt'])
                            if (len(acf)>iwindow-1):
                                AUTOCORREL[-1][:len(acf)] += acf[:len(AUTOCORREL[-1])]
                                COUNT[-1] += 1
                hist, be = np.histogram(Vm, bins=bins, normed=True)
                HIST.append(hist)
            HISTS.append(HIST)
            AUTOCORRELS.append(AUTOCORREL)
            COUNTS.append(COUNT)
            ax.plot(.5*(be[1:]+be[:-1]), hist, color=color, lw=2, label=lab)
        np.save('sparse_vs_balanced/data/analyzed_Vm_hist.npy', [np.array(HISTS), bins])
        np.save('sparse_vs_balanced/data/analyzed_autocorrel.npy', [np.array(AUTOCORRELS), shift, np.array(COUNTS)])
        

    return fig4, fig5

def run_sim(Model, SEED=4):

    tstop = Model['rise']+3*(3.*Model['rise']+Model['DT'])
    NTWK = run_3pop_ntwk_model(Model,
                               faff_waveform_func=waveform,
                               with_Vm=10,
                               filename=Model['filename'], tstop=tstop, SEED=SEED)
    return NTWK

def run_scan(Model):
    
    tstop = Model['rise']+3*(3.*Model['rise']+Model['DT'])
    
    zf = zipfile.ZipFile(Model['zip_filename'], mode='w')

    seeds = Model['SEEDS']
    Model['FILENAMES'] = np.empty(len(seeds), dtype=object)
    
    for j in range(len(seeds)):
        fn = Model['data_folder']+str(seeds[j])+\
             '_'+str(np.random.randint(100000))+'.h5'
        Model['FILENAMES'][j] = fn
        print('running configuration ', fn)
        run_3pop_ntwk_model(Model,
                            faff_waveform_func=waveform,
                            with_Vm=500,
                            filename=fn, tstop=tstop,
                            SEED=seeds[j])
        zf.write(fn)
        
    # writing the parameters
    np.savez(Model['zip_filename'].replace('.zip', '_Model.npz'), **Model)
    zf.write(Model['zip_filename'].replace('.zip', '_Model.npz'))

    zf.close()

if __name__=='__main__':
    
    # import the model defined in root directory
    sys.path.append(str(pathlib.Path(__file__).resolve().parents[2]))
    from model import *

    # parameters of the input
    parser.add_argument('--Faff1', type=float, default=4.)    
    parser.add_argument('--Faff2', type=float, default=18.)    
    parser.add_argument('--Faff3', type=float, default=8.)    
    parser.add_argument('--DT', type=float, default=900)    
    parser.add_argument('--rise', type=float, default=50)    
    
    parser.add_argument('--SEEDS', nargs='+',
                        help='various seeds', type=int,
                        default=np.arange(4))    
    # additional stuff
    parser.add_argument('-df', '--data_folder',
                        help='Folder for data', default='data/')    
    parser.add_argument("--zip_filename", '-f',
                        help="filename for the zip file",type=str,
                        default='data/time_varying_input.zip')
    parser.add_argument("-a", "--analyze", help="perform analysis of params space",
                        action="store_true")
    parser.add_argument("-rm", "--run_multiple_seeds", help="run with multiple seeds",
                        action="store_true")
    parser.add_argument("--show_input", action="store_true")
    parser.add_argument("--debug", help="debug", action="store_true")
    
    args = parser.parse_args()
    Model = vars(args)
    Model['p_AffExc_DsInh'] = 0.075
    Model['p_DsInh_RecInh'] = 0.05

    if args.analyze:
        analyze_sim(Model)
        ntwk.show()
    elif args.show_input:
        aff_pop_time_course(Model)
        show()
    elif args.run_multiple_seeds:
        Model['filename'] = 'sparse_vs_balanced/data/time_varying_input.h5'
        run_scan(Model)
    elif args.debug:
        Model, seeds, DATA = get_scan({},
            filename='sparse_vs_balanced/data/time_varying_input.zip')
        # conductance_time_course(DATA, Model)
        Model, DATA = None, None
        hist_of_Vm(DATA, Model, plot_only=True)
        # fig = conductance_time_course(DATA, Model, plot_only=True)
        # fig, fig2 = hist_of_Vm(DATA, Model, plot_only=True)
        # fig.savefig('/Users/yzerlaut/Desktop/temp.svg')
        # fig2.savefig('/Users/yzerlaut/Desktop/temp2.svg')
        ntwk.show()
    else:
        Model['filename'] = 'data/time_varying_input_debug.h5'
        NTWK = run_sim(Model)
        Nue, Nui, Nud = NTWK['POP_ACT'][0].rate/ntwk.Hz, NTWK['POP_ACT'][1].rate/ntwk.Hz,\
                        NTWK['POP_ACT'][2].rate/ntwk.Hz
        ntwk.plot(NTWK['POP_ACT'][0].t/ntwk.ms, gaussian_smoothing(Nue,int(2./0.1)))
        ntwk.plot(NTWK['POP_ACT'][0].t/ntwk.ms, gaussian_smoothing(Nui,int(2./0.1)))
        ntwk.plot(NTWK['POP_ACT'][0].t/ntwk.ms, gaussian_smoothing(Nud,int(2./0.1)))
        plt.yscale('log')
        ntwk.show()

