import sys, pathlib, os
sep = os.path.sep # Ms-Win vs UNIX
sys.path.append(str(pathlib.Path(__file__).resolve().parents[1]))
import numpy as np
import pymuvr
from sklearn.neighbors import KNeighborsClassifier
from scipy.stats import ttest_rel
from sparse_vs_balanced.sparse_spiking_input import get_scan


Patterns, seeds, _, DATA_SA = get_scan({}, filename='sparse_vs_balanced/data/sparse_input_SA.zip')
F_aff, seeds, Model, DATA_BA = get_scan({}, filename='sparse_vs_balanced/data/sparse_input_BA.zip')
PRESYNAPTIC_PATTERNS_TRAINING = []
# each element will be a RASTER corresponding to one pattern and one seed !
SA_PATTERNS_TRAINING, BA_PATTERNS_TRAINING = [], []

# we will also get the (trial-invariants) input patterns
PRESYNAPTIC_PATTERNS_RASTER = []

Ntraining = int(len(seeds)/3)
Ntest = len(seeds)-Ntraining

t_pre, t_post = 100, 100 # we also considered 100 ms around the stim
for i in range(len(Patterns)):
    # TRAINING SET
    for j in range(Ntraining): # seeds only in the first training seet
        # first loop for training
        for DATA, PATTERNS in zip([DATA_SA, DATA_BA], [SA_PATTERNS_TRAINING, BA_PATTERNS_TRAINING]):
            data = DATA[i*len(seeds)+j]
            cond = (data['tRASTER_RecExc']>Model['t0']-t_pre) &\
                (data['iRASTER_RecExc']<int(Model['N_target'])) &\
                (data['tRASTER_RecExc']<Model['t0']+Model['time_span']+t_post)
            RASTER = []
            for ii in np.arange(int(Model['N_target'])):
                Nrn = []
                i0 = np.argwhere(data['iRASTER_RecExc'][cond]==ii).flatten()
                Nrn += list(data['tRASTER_RecExc'][cond][i0].flatten())
                RASTER.append(Nrn)
            PATTERNS.append(RASTER)
        PRESYNAPTIC_PATTERNS_TRAINING.append(i)

def compare_two_regimes(filenameSA2, filenameBA2):                 

    Patterns2, seeds, Model, DATA_SA2 = get_scan({}, filename=filenameSA2)
    Patterns2, seeds, Model, DATA_BA2 = get_scan({}, filename=filenameBA2)

    PRESYNAPTIC_PATTERNS_TEST = []
    # each element will be a RASTER corresponding to one pattern and one seed !
    SA_PATTERNS_TEST, BA_PATTERNS_TEST = [], []

    # we will also get the (trial-invariants) input patterns
    PRESYNAPTIC_PATTERNS_RASTER_TEST = []

    for i in range(len(Patterns2)):
        # TEST SET
        for j in range(len(seeds)): # seeds only in the test set
            # second loop for tests
            for DATA, PATTERNS in zip([DATA_SA2, DATA_BA2], [SA_PATTERNS_TEST, BA_PATTERNS_TEST]):
                data = DATA[i*len(seeds)+j]
                cond = (data['tRASTER_RecExc']>Model['t0']-t_pre) & (data['iRASTER_RecExc']<int(Model['N_target'])) &\
                                      (data['tRASTER_RecExc']<Model['t0']+Model['time_span']+t_post)
                RASTER = []
                for ii in np.arange(int(Model['N_target'])):
                    Nrn = []
                    i0 = np.argwhere(data['iRASTER_RecExc'][cond]==ii).flatten()
                    Nrn += list(data['tRASTER_RecExc'][cond][i0].flatten())
                    RASTER.append(Nrn)
                PATTERNS.append(RASTER)
            PRESYNAPTIC_PATTERNS_TEST.append(i) # A simple index accounts for the 
        ## AFFERENT SPIKES
        ## We also collect the afferent stimulus (identical on all seeds, so we take it on the last 'j' seed)
        RASTER = []
        for n in np.arange(Model['N_target']):
            Nrn = []
            i0 = np.argwhere(data['iRASTER_PRE_in_terms_of_Pre_Pop'][0]==n).flatten()
            Nrn += list(data['tRASTER_PRE_in_terms_of_Pre_Pop'][0][i0].flatten())
            RASTER.append(Nrn)
        PRESYNAPTIC_PATTERNS_RASTER_TEST.append(RASTER)

    """
    A distance based on the Multi-Unit Van Rossum metrics
    """
    def spike_train_distance(PATTERNS, cos=0.1, tau=5):
        return pymuvr.square_dissimilarity_matrix(PATTERNS,\
                                                  cos, tau, 'distance')[0,1]
    """
    Desiging a custom 'metric' function that fits the datatype of 'sklearn.neighbors.KNeighborsClassifier'
    """
    # for the sparse activity data
    def my_func_SA(X1, X2):
        flag1, index1 = X1
        flag2, index2 = X2
        # constructing RASTER1
        if flag1==1: RASTER1 = SA_PATTERNS_TEST[int(index1)] # test flag
        elif flag1==0: RASTER1 = SA_PATTERNS_TRAINING[int(index1)] # training flag
        else: RASTER1 = None # this will raise an error
        # constructing RASTER2
        if flag2==1: RASTER2 = SA_PATTERNS_TEST[int(index2)] # test flag
        elif flag2==0: RASTER2 = SA_PATTERNS_TRAINING[int(index2)] # training flag
        else: RASTER2 = None # this will raise an error

        return spike_train_distance([RASTER1, RASTER2])

    # for the balanced activity data
    def my_func_BA(X1, X2):
        flag1, index1 = X1
        flag2, index2 = X2
        # constructing RASTER1
        if flag1==1: RASTER1 = BA_PATTERNS_TEST[int(index1)] # test flag
        elif flag1==0: RASTER1 = BA_PATTERNS_TRAINING[int(index1)] # training flag
        else: RASTER1 = None # this will raise an error
        # constructing RASTER2
        if flag2==1: RASTER2 = BA_PATTERNS_TEST[int(index2)] # test flag
        elif flag2==0: RASTER2 = BA_PATTERNS_TRAINING[int(index2)] # training flag
        else: RASTER2 = None # this will raise an error

        return spike_train_distance([RASTER1, RASTER2])
    """
    performing the classification for Sparse Activity
    """
    from sklearn.neighbors import KNeighborsClassifier
    neigh = KNeighborsClassifier(n_neighbors=1, metric=my_func_SA)
    # neigh = KNeighborsClassifier(n_neighbors=1, metric=pymuvr.square_dissimilarity_matrix)
    neigh.fit([np.array([0, i]) for i in range(len(SA_PATTERNS_TRAINING))], PRESYNAPTIC_PATTERNS_TRAINING)
    SA_PREDICTED = neigh.predict([[1, i] for i in range(len(SA_PATTERNS_TEST))])
    """
    performing the classification for Balanced Activity
    """
    neigh = KNeighborsClassifier(n_neighbors=1, metric=my_func_BA)
    neigh.fit([np.array([0, i]) for i in range(len(BA_PATTERNS_TRAINING))], PRESYNAPTIC_PATTERNS_TRAINING)
    BA_PREDICTED = neigh.predict([[1, i] for i in range(len(BA_PATTERNS_TEST))])

    BA_accuracy, SA_accuracy = [], []
    for k in np.unique(PRESYNAPTIC_PATTERNS_TEST):
        i0 = np.argwhere(np.array(PRESYNAPTIC_PATTERNS_TEST)==k).flatten()
        # print(i0, k)
        BA_accuracy.append(0)
        SA_accuracy.append(0)
        for i in i0:
            if PRESYNAPTIC_PATTERNS_TEST[i]==BA_PREDICTED[i]:
                BA_accuracy[-1] += 1./len(i0)
            if PRESYNAPTIC_PATTERNS_TEST[i]==SA_PREDICTED[i]:
                SA_accuracy[-1] += 1./len(i0)
    res = ttest_rel(SA_accuracy, BA_accuracy)
    return SA_accuracy, BA_accuracy, res

SAA, BAA, P = [], [], []
X = [0., 2., 5., 20.]
for suffix in ['.zip', '_ts_2.zip', '_ts_5.zip', '_ts_20.zip']:
    SA_accuracy, BA_accuracy, res = compare_two_regimes('sparse_vs_balanced/data/sparse_input_SA'+suffix,\
                                                        'sparse_vs_balanced/data/sparse_input_BA'+suffix)
    SAA.append(SA_accuracy)
    BAA.append(BA_accuracy)
    P.append(res.pvalue)

np.save('sparse_vs_balanced/data/analyzed_temporal_structure.npy', [SAA, BAA, P])    
