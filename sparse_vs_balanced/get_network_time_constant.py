import numpy as np
from itertools import product
import sys, pathlib, os
sys.path.append(str(pathlib.Path(__file__).resolve().parents[1]))
import neural_network_dynamics.main as ntwk
from sparse_vs_balanced.running_2pop_model import run_2pop_ntwk_model
from sparse_vs_balanced.running_3pop_model import run_3pop_ntwk_model
from graphs.my_graph import *
from matplotlib import ticker
# everything stored within a zip file
from scipy.special import erf
from data_analysis.processing.signanalysis import gaussian_smoothing, autocorrel
from graphs.plot_export import put_list_of_figs_to_svg_fig
from matplotlib.cm import copper
import zipfile
from data_analysis.IO.hdf5 import load_dict_from_hdf5
from data_analysis.optimization.fits import leastsq_fit

Blue, Orange, Green, Red, Purple, Brown, Pink, Grey,\
    Kaki, Cyan = '#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',\
    '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf'
Inter = get_linear_colormap(Blue, Orange)(.5) # for the intermediate regime

def waveform(t, Model):
    waveform = 0*t+Model['F_AffExc']
    for tt in np.arange(Model['Nrepeat'])*3*Model['DT']:
        cond = (t>Model['t0']+tt) & (t<Model['t0']+tt+Model['DT'])
        waveform[cond] = Model['Faff1']+Model['F_AffExc']
    return waveform

def aff_pop_time_course(Model, t0=400.,
                        FIGSIZE=(8,2),
                        YTICKS=[0, 5, 10], XBAR=500):
    
    tstop = Model['t0']+3*Model['Nrepeat']*Model['DT']
    t = np.arange(int(tstop/Model['dt']))*Model['dt']
    faff = waveform(t, Model)
    
    fig3, ax = plt.subplots(1, figsize=FIGSIZE)
    plt.subplots_adjust(bottom=0.3)
    plt.plot(t, faff, 'k-')
    plt.plot([100, 100+XBAR], [2., 2.], color='gray', lw=3)
    plt.annotate(str(int(XBAR))+'ms', (150, 2.5))
    set_plot(ax, ['left'], ylabel='$\\nu_a$ (Hz)',
             yticks=YTICKS, xticks=[], xlim=[0, tstop])
    return fig3

def get_scan(Model, with_png_export=False,
             filename=None):

    if filename is None:
        filename=str(Model['zip_filename'])
    zf = zipfile.ZipFile(filename, mode='r')
    
    data = zf.read(filename.replace('.zip', '_Model.npz'))
    with open(filename.replace('.zip', '_Model.npz'), 'wb') as f: f.write(data)
    Model = dict(np.load(filename.replace('.zip', '_Model.npz')).items())
    
    seeds = Model['SEEDS']
    
    DATA = []
    for j in range(len(seeds)):
        fn = Model['FILENAMES'][j]
        data = zf.read(fn)
        with open(fn, 'wb') as f: f.write(data)
        with open(fn, 'rb') as f: data = ntwk.load_dict_from_hdf5(fn)
        DATA.append(data)
    return Model, seeds, DATA

def pop_act_time_course(DATA, Model, t0=0., smoothing=10,
                        FIGSIZE=(4,2), lower_lim=0.01,
                        YTICKS=[0.01, 1., 100.],
                        YTICKS_LABELS=['<0.01', '1', '100']):
    
    tstop = Model['t0']+3*args.Nrepeat*Model['DT']
    t = np.arange(int(tstop/Model['dt']))*Model['dt']
    ismooth = int(smoothing/Model['dt'])
    MEAN_FE, MEAN_FI, MEAN_FD = [np.zeros((len(DATA), len(t))) for i in range(3)]
    
    for n, data in enumerate(DATA):
        
        MEAN_FE[n, :] = gaussian_smoothing(data['POP_ACT_RecExc'], ismooth)
        MEAN_FI[n, :] = gaussian_smoothing(data['POP_ACT_RecInh'], ismooth)
        MEAN_FD[n, :] = gaussian_smoothing(data['POP_ACT_DsInh'], ismooth)

    faff = waveform(t, Model)
    cond = (t>t0) & (t<20)
    
    fig3, ax = plt.subplots(1, figsize=FIGSIZE)

    for vec, label, color in zip([MEAN_FI, MEAN_FE, MEAN_FD],
                                 ['$\\nu_i$', '$\\nu_e$', '$\\nu_d$'],
                                 [Red, Green, Purple]):
        y = vec.mean(axis=0)
        sy = vec.std(axis=0)
        minus_y, plus_y = y-sy, y+sy
        y[y<lower_lim] = lower_lim
        minus_y[minus_y<lower_lim] = lower_lim
        plus_y[plus_y<lower_lim] = lower_lim
        plt.plot(t[cond], y[cond], color=color, label=label, lw=2)
        plt.fill_between(t[cond], minus_y[cond], plus_y[cond],
                         color=color, alpha=.5, lw=0)

    ax.set_yscale('log')
    ax.legend(frameon=False)
    set_plot(ax, ['left'],
             yticks=YTICKS, yticks_labels=YTICKS_LABELS, 
             ylabel='rate (Hz)', xticks=[], xlim=[t0, tstop])
    return fig3

def compute_resp(data, Taround=100, smooth=3):

    t0, length = data['t0'], data['DT']
    tzoom = np.arange(int((data['DT']+2*Taround)/data['dt']))*data['dt']-Taround
    # tzoom = np.arange(int((2*data['DT']+2*Taround)/data['dt']))*data['dt']-Taround
    output = 0*tzoom
    for tt in np.arange(data['Nrepeat'])*3*length:
        it = int((tt+t0-Taround)/data['dt'])
        output += gaussian_smoothing(data['POP_ACT_RecExc'][it:it+len(tzoom)]/float(data['Nrepeat']), int(smooth/data['dt']))
    print('Levels of discarded background activity:', np.mean(output[tzoom<0]))
    output -= np.mean(output[tzoom<0])
    return tzoom, output
    
    
def analyze_sim(Model, separation_factor=1.4):

    fig, ax0 = plt.subplots(1, figsize=(2.5,1.8))
    plt.subplots_adjust(left=.2, bottom=.3, right=.5)
    ax = plt.axes([.5,.4,.4,.6])
    for j in range(len(Model['Fbg'])):
        print('analyzing configuration ', Model['Fbg'][j])
        data = load_dict_from_hdf5(Model['data_folder']+'Tntwk_Fa_'+str(Model['Fbg'][j])+'.h5')
        print('-------------> Nrepeat', data['Nrepeat'])
        tzoom, output = compute_resp(data, Taround=Model['Taround'], smooth=Model['Tsmoothing'])
        # print(data['DT'])
        # data['DT'] *= 2
        def waveform(tzoom, Coeffs, Taround=Model['Taround'], Tlength=data['DT']):
            amplitude, Tau = Coeffs
            output = 0*tzoom
            output[(tzoom>0) & (tzoom<Tlength)] = amplitude*(1-np.exp(-tzoom[(tzoom>0) & (tzoom<Tlength)]/Tau))
            output[tzoom>=Tlength] = amplitude*np.exp(-(tzoom[tzoom>=Tlength]-Tlength)/Tau)
            return output
        C = leastsq_fit(tzoom, output, waveform, [1, 10])
        cond_fit_plot = tzoom<1e6
        ax.plot(tzoom, -separation_factor*j+output/C[0], color=[Blue, Inter, Orange][j], lw=1)
        ax.plot(tzoom[cond_fit_plot], -separation_factor*j+waveform(tzoom[cond_fit_plot], C)/C[0],
                '--', color=[Blue, Inter, Orange][j], lw=1)
        ax.plot(tzoom[cond_fit_plot], -separation_factor*j+waveform(tzoom[cond_fit_plot], C)/C[0],
                '--', color='r', lw=1)
        ax.annotate(str(np.round(np.abs(C[0]),1))+'Hz', (-Model['Taround'], 0), color=[Blue, Inter, Orange][j])
        ax0.bar([j], [C[1]], color=[Blue, Inter, Orange][j], width=.6)
        print('time constant: ', C[1], 'ms')
    Input = 0*tzoom
    Input[(tzoom>0) & (tzoom<data['DT'])] = 1
    ax.plot(tzoom, separation_factor+Input, 'k-', lw=1)
    ax.annotate(str(round(float(data['Faff1']),1))+'Hz', (-Model['Taround'], 0))
    ax.annotate(str(Model['Tbar'])+'ms', (-Model['Taround'], 1))
    ax.plot(-Model['Taround']*np.ones(2), [0,1], 'k-', lw=1)
    ax.plot(-Model['Taround']+np.arange(2)*Model['Tbar'], [1,1], 'k-', lw=1)
    set_plot(ax,[],xticks=[],yticks=[])
    set_plot(ax0, xticks=[0,1,2], xticks_labels=['$T_1$', '$T_2$', '$T_3$'], ylabel=r'$\tau_{NTWK}$ (ms)')
    
    return fig
    
def run_sim(Model, SEED=4):

    tstop = Model['t0']+3*(args.Nrepeat+1)*Model['DT']
    NTWK = run_3pop_ntwk_model(Model,
                               faff_waveform_func=waveform,
                               with_Vm=0, # no Vm !
                               filename=Model['filename'],
                               tstop=tstop, SEED=SEED)
    return NTWK



def run_scan(Model):
    
    for j in range(len(Model['Fbg'])):
        print('running configuration ', Model['Fbg'][j])
        Model['F_AffExc'] = Model['Fbg'][j]
        Model['filename'] = Model['data_folder']+'Tntwk_Fa_'+str(Model['Fbg'][j])+'.h5'
        run_sim(Model, SEED=34)


if __name__=='__main__':
    
    # import the model defined in root directory
    sys.path.append(str(pathlib.Path(__file__).resolve().parents[2]))
    from model import *

    # parameters of the input
    parser.add_argument('--Faff1', type=float, default=2.)    
    parser.add_argument('--Fbg', type=float, nargs='*', default=[4, 8, 18])    
    parser.add_argument('--DT', type=float, default=100)    
    parser.add_argument('--t0', type=float, default=400)    
    parser.add_argument('--Taround', type=float, default=90)    
    parser.add_argument('--Tsmoothing', type=float, default=1)    
    parser.add_argument('--Tbar', type=int, default=50)    
    parser.add_argument('--Nrepeat', type=int, default=100)    
    
    parser.add_argument('--SEEDS', nargs='+',
                        help='various seeds', type=int,
                        default=np.arange(4))    
    # additional stuff
    parser.add_argument('-df', '--data_folder',
                        help='Folder for data', default='data/')    
    parser.add_argument("--zip_filename",
                        help="filename for the zip file",type=str,
                        default='data/time_varying_input.zip')
    parser.add_argument("--filename", '-f',
                        help="filename for data zip file",type=str,
                        default='data/data/get_time_cst.h5')
    parser.add_argument("-a", "--analyze", help="perform analysis of params space",
                        action="store_true")
    parser.add_argument("-rs", "--run_scan", help="run with multiple seeds",
                        action="store_true")
    parser.add_argument("--show_input", action="store_true")
    parser.add_argument("--debug", help="debug", action="store_true")
    
    args = parser.parse_args()
    Model = vars(args)
    Model['p_AffExc_DsInh'] = 0.075
    Model['p_DsInh_RecInh'] = 0.05

    if args.analyze:
        fig = analyze_sim(Model)
        fig.savefig(desktop+'fig.svg')
        ntwk.show()
    elif args.show_input:
        aff_pop_time_course(Model)
        show()
    elif args.run_scan:
        run_scan(Model)
    elif args.debug:
        # Model, seeds, DATA = get_scan({},
        #     filename='sparse_vs_balanced/data/Tntwk_Fa_8.zip')
        pop_act_time_course(DATA, Model)
        # conductance_time_course(DATA, Model)
        Model, DATA = None, None
        hist_of_Vm(DATA, Model, plot_only=True)
        ntwk.show()
    else:
        NTWK = run_sim(Model)
        # Nue, Nui, Nud = NTWK['POP_ACT'][0].rate/ntwk.Hz, NTWK['POP_ACT'][1].rate/ntwk.Hz,\
        #                 NTWK['POP_ACT'][2].rate/ntwk.Hz
        # ntwk.plot(NTWK['POP_ACT'][0].t/ntwk.ms, gaussian_smoothing(Nue,int(2./0.1)))
        # ntwk.plot(NTWK['POP_ACT'][0].t/ntwk.ms, gaussian_smoothing(Nui,int(2./0.1)))
        # ntwk.plot(NTWK['POP_ACT'][0].t/ntwk.ms, gaussian_smoothing(Nud,int(2./0.1)))
        # plt.yscale('log')
        # ntwk.show()

