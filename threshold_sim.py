import numpy as np
import os, sys
from model import Model
from graphs.my_graph import *

def my_logspace(x1, x2, n):
    return np.logspace(np.log(x1)/np.log(10), np.log(x2)/np.log(10), n)


Q_FACTOR = [0.1, 1., 5.] # chosen values to investigate

N = 30
FE = [my_logspace(9, 200, N),\
      my_logspace(0.5, 30, N),\
      my_logspace(0.05, 30, N)]
      
Desired_muV = np.linspace(-65, -52, N)*1e-3

N2 = 500
FI = my_logspace(1e-2,1000,N2)

Model2 = Model.copy()
Model2['POP_STIM'] = ['RecExc', 'RecInh']
Model2['NRN_KEY'] = 'RecExc'
Model2['N_SEED'] = 1
Model2['tstop'] = 1000.
Model2['dt'] = 0.1
# Model2['alpha_RecExc_RecExc'] = 0.
# Model2['alpha_RecExc_RecInh'] = 0.
# Model2['alpha_RecInh_RecInh'] = 0.
# Model2['alpha_RecInh_RecExc'] = 0.
Model2['V0'] = -70.

def make_Vm_and_hist_plot(fe, fi, ax1, ax2, max_view=1000, Vm_lim=[-80, -20]):
    Model2['RATES'] = {'F_RecExc':fe, 'F_RecInh':fi}
    # subthreshold
    Model2['RecExc_Vthre'] = +500
    data = run_sim(Model2, with_Vm=1)
    t = np.arange(len(data['Vm'][0]))*Model['dt']
    ax1.plot(t[t<max_view], data['Vm'][0][t<max_view], color=Blue, lw=1, label='without threshold')
    ax2.hist(data['Vm'][0][data['Vm'][0] != -70], bins=30, color=Blue, density=True)
    # suprathreshold
    Model2['RecExc_Vthre'] = -50
    data = run_sim(Model2, with_Vm=1)
    ax1.plot(t[t<max_view], data['Vm'][0][t<max_view], 'r-', lw=1, label='with threshold')
    ax2.hist(data['Vm'][0][data['Vm'][0] != -70], bins=30, color='r', alpha=.5, density=True)
    set_plot(ax1, xlabel='time (ms)', ylabel='Vm (mV)')
    set_plot(ax2, xlabel='Vm (mV)', ylabel='count', yticks=[], xlim=Vm_lim)
    ax2.set_title('$\\nu_e$=%.1f, $\\nu_i$=%.1f' % (fe, fi))


def evaluate_Vm_fluctuations(data,t_init=100, repolarization_blank=1, Trefrac=5):
    t = np.arange(len(data['Vm'][0,:]))*Model2['dt']
    muV, sV, gV, Tv = [], [], [], []
    for i in range(data['Vm'].shape[0]):
        tspikes = data['tspikes'][np.argwhere(data['ispikes']==i).flatten()]
        cond = (t>t_init)
        for tt in tspikes:
            cond = cond & np.invert((t>tt) & (t<tt+Trefrac+repolarization_blank))
        muV.append(np.mean(data['Vm'][i,:]))
        sV.append(np.std(data['Vm'][i,:]))
        gV.append(skew(data['Vm'][i,:]))
        Tv.append(0)
    return np.array(muV), np.array(sV), np.array(gV), np.array(Tv)
        
if sys.argv[-1]=='run':
    
    from model import Model, pass_arguments_of_new_model
    from neural_network_dynamics.transfer_functions.single_cell_protocol import run_sim, my_logspace
    from neural_network_dynamics.cells.cell_library import built_up_neuron_params
    from neural_network_dynamics.theory.Vm_statistics import getting_statistical_properties
    from neural_network_dynamics.theory.tf import build_up_afferent_synaptic_input
    from scipy.stats import skew
    
    output = {'fout':np.zeros((N, len(Q_FACTOR))),
              'sfout':np.zeros((N, len(Q_FACTOR))),
              'FI':np.zeros((N, len(Q_FACTOR))),
              'FE':np.zeros((N, len(Q_FACTOR))),
              'SVth':np.zeros((N, len(Q_FACTOR))),
              'GV':np.zeros((N, len(Q_FACTOR))),
              'MUVsub':np.zeros((N, len(Q_FACTOR))),
              'sMUVsub':np.zeros((N, len(Q_FACTOR))),
              'SVsub':np.zeros((N, len(Q_FACTOR))),
              'sSVsub':np.zeros((N, len(Q_FACTOR))),
              'MUV':np.zeros((N, len(Q_FACTOR))),
              'sMUV':np.zeros((N, len(Q_FACTOR))),
              'SV':np.zeros((N, len(Q_FACTOR))),
              'sSV':np.zeros((N, len(Q_FACTOR)))}
    for iq, q in enumerate(Q_FACTOR):
        output['FE'][:, iq] = FE[iq]
        for e, fe in enumerate(FE[iq]):
            for key in ['Q_RecExc_RecExc', 'Q_RecExc_RecInh', 'Q_RecInh_RecExc', 'Q_RecInh_RecInh']:
                Model2[key] = Model[key]*q
            SYN_POPS = build_up_afferent_synaptic_input(Model2, ['RecExc', 'RecInh'], NRN_KEY='RecExc')
            muV, sV, gV, Tv = getting_statistical_properties(built_up_neuron_params(Model2, 'RecExc'),
                                                             SYN_POPS, {'F_RecExc':fe, 'F_RecInh':FI})
            i0 = np.argmin((muV-Desired_muV[e])**2)
            Model2['RATES'] = {'F_RecExc':fe, 'F_RecInh':FI[i0]}
            output['SVth'][e, iq] = sV[i0]
            output['FI'][e, iq] = FI[i0]
            # # WITH THRESHOLD
            # Model2['RecExc_Vthre'] = -50 # with threshold
            # # output['fout'][e, iq], output['sfout'][e, iq] = run_sim(Model2, firing_rate_only=True)
            # data = run_sim(Model2, firing_rate_only=False, with_Vm=Model2['N_SEED'])
            # muV, sV, gV, Tv = evaluate_Vm_fluctuations(data)
            # output['MUV'][e, iq] = np.mean(muV)
            # output['sMUV'][e, iq] = np.std(muV)
            # output['SV'][e, iq] = np.mean(sV)
            # output['sSV'][e, iq] = np.std(sV)
            # data = run_sim(Model2, firing_rate_only=False, with_Vm=Model2['N_SEED'])
            # # SUBTHRESHOLD
            # Model2['RecExc_Vthre'] = +500 # only subthreshold dynamics
            # data = run_sim(Model2, firing_rate_only=False, with_Vm=Model2['N_SEED'])
            # muV, sV, gV, Tv = evaluate_Vm_fluctuations(data)
            # output['MUVsub'][e, iq] = np.mean(muV)
            # output['sMUVsub'][e, iq] = np.std(muV)
            # output['SVsub'][e, iq] = np.mean(sV)
            # output['sSVsub'][e, iq] = np.std(sV)
            
    np.savez('sparse_vs_balanced/data/threshold_data.npz', **output)
    
elif sys.argv[-1]=='plot':
    output = dict(np.load('sparse_vs_balanced/data/threshold_data.npz'))
    fig, AX = figure(axes=(4,3))
    AX = np.array(AX)
    for a, ax in enumerate(AX[3]):
        plot(output['FE'][:,a], y=output['FI'][:,a], color=viridis(a/2.), ax=ax, lw=2)
        set_plot(ax, xscale='log', xlabel='$\\nu_e$ (Hz)', yscale='log', xlim=[FE[a][0],FE[a][-1]])
    for a, ax in enumerate(AX[2]):
        ax.plot(output['FE'][:,a], 1e3*Desired_muV, 'k:', lw=3, label='desired $\mu_V$', alpha=.5)
        plot(output['FE'][:,a], y=output['MUV'][:,a], sy=output['sMUV'][:,a], color=viridis(a/2.), ax=ax, lw=2)
        set_plot(ax, xscale='log', xlim=[FE[a][0],FE[a][-1]])
    AX[2][0].legend(frameon=False, prop={'size':'small'})
    for a, ax in enumerate(AX[1]):
        ax.plot(output['FE'][:,a], 1e3*output['SVth'][:,a], 'k-', lw=3, label='analytical estimate \n(subthreshold dyn.)', alpha=.5)        
        plot(output['FE'][:,a], y=output['SV'][:,a], sy=output['sSV'][:,a], color=viridis(a/2.), ax=ax, lw=2)
        set_plot(ax, xscale='log', xlim=[FE[a][0],FE[a][-1]])
    AX[1][1].legend(frameon=False, prop={'size':'small'})
    for a, ax in enumerate(AX[0]):
        plot(output['FE'][:,a], y=output['fout'][:,a]+1e-2, sy=output['sfout'][:,a], color=viridis(a/2.), ax=ax, lw=2)
        set_plot(ax, xscale='log', yscale='log', xlim=[FE[a][0],FE[a][-1]])
    AX[0][0].set_ylabel('$\\nu_{out}$ (Hz)')
    AX[1][0].set_ylabel('$\\sigma_V$ (mV)')
    AX[2][0].set_ylabel('$\\mu_V$ (mV)')
    AX[3][0].set_ylabel('$\\nu_{i}$ (Hz)')
    show()
elif sys.argv[-1]=='demo':

    from model import Model, pass_arguments_of_new_model
    from neural_network_dynamics.transfer_functions.single_cell_protocol import run_sim, my_logspace
    from neural_network_dynamics.cells.cell_library import built_up_neuron_params
    from neural_network_dynamics.theory.Vm_statistics import getting_statistical_properties
    from neural_network_dynamics.theory.tf import build_up_afferent_synaptic_input
    from scipy.stats import skew
    
    # demo simulation
    Model2['N_SEED'] = 1
    Model2['N'] = 1
    Model2['tstop'] = 100
    
    fig, AX1 = figure(axes_extents=[[[2,1]],[[2,1]],[[2,1]]])
    fig, AX2 = figure(axes_extents=[[[1,1]],[[1,1]],[[1,1]]])
    FE = np.array([0.1, 1., 10.])
    FI = 3.*FE
    for i, ax in enumerate(AX1):
        Model2['RATES'] = {'F_RecExc':FE[i], 'F_RecInh':FI[i]}
        make_Vm_and_hist_plot(FE[i], FI[i], AX1[i], AX2[i], max_view=2000)
    show()

else:

    from model import Model, pass_arguments_of_new_model
    from neural_network_dynamics.transfer_functions.single_cell_protocol import run_sim, my_logspace
    from neural_network_dynamics.cells.cell_library import built_up_neuron_params
    from neural_network_dynamics.theory.Vm_statistics import getting_statistical_properties
    from neural_network_dynamics.theory.tf import build_up_afferent_synaptic_input
    from scipy.stats import skew
    
    # demo simulation
    Model2['N_SEED'] = 1
    Model2['N'] = 1
    Model2['tstop'] = 2000
        
    fig1, ax1 = figure(axes_extents=[[[2,1]]])
    fig2, ax2 = figure()
    make_Vm_and_hist_plot(.2, .4, ax1, ax2, max_view=2000, Vm_lim=[-80, 20])
    show()
    fig1.savefig(desktop+'temp1.svg')
    fig2.savefig(desktop+'temp2.svg')

    
