# import numpy as np
# import os, sys

from graphs.my_graph import *

X = np.load('sparse_vs_balanced/data/varying_AffExc_analyzed.npy')
fig, [ax, ax2]= figure(axes=(1,2), wspace=3.)
Fa, Fe, sFe = np.unique(X[0]), [], []
for fa in Fa:
    i0 = np.argwhere(X[0]==fa).flatten()
    Fe.append(np.mean(X[4][i0]))
    sFe.append(np.std(X[4][i0]))
ax.errorbar(Fa, Fe, yerr=sFe, color=Green, lw=1, ms=3)
set_plot(ax, xscale='log', yscale='log', xlabel='$\\nu_a$ (Hz)', ylabel='$\\nu_e$ (Hz)',
         xticks=[5, 10, 20], xticks_labels=['5', '10', '20'])

iAD, lAD = 2, 8
iRD, lRD = 23, 3
ax.plot([Fa[iAD]], [Fe[iAD]], 'o', color=Blue)
ax.plot([Fa[iRD]], [Fe[iRD]], 'o', color=Orange)

from scipy.optimize import minimize
ax2.errorbar(Fa[iAD:iAD+lAD]-Fa[iAD], Fe[iAD:iAD+lAD]-Fe[iAD], yerr=sFe[iAD:iAD+lAD], fmt='o', color=Blue, lw=0.5,ms=2)
def to_minimize(x):
    return np.sum((Fe[iAD:iAD+lAD]-Fe[iAD]-x*(Fa[iAD:iAD+lAD]-Fa[iAD]))**2)
res = minimize(to_minimize, 1.)
ax2.plot([0,2.8], res.x*np.array([0,2.8]), color=Blue)
ax2.errorbar(Fa[iRD:iRD+lRD]-Fa[iRD],Fe[iRD:iRD+lRD]-Fe[iRD], yerr=sFe[iRD:iRD+lRD], fmt='o', color=Orange, lw=0.5,ms=2)
ax2.plot([0,3], np.polyval(np.polyfit(Fa[iRD:iRD+lRD]-Fa[iRD], Fe[iRD:iRD+lRD]-Fe[iRD], 2), [0,3]), color=Orange)
set_plot(ax2, xlabel='$\delta \\nu_a$ (Hz)', ylabel='$\delta \\nu_e$ (Hz)')

fig.savefig(desktop+'fig.png', dpi=200)
# lAD = 12
# lRD = 5
# ax3.errorbar(Fa[iAD:iAD+lAD]-Fa[iAD], Fe[iAD:iAD+lAD]-Fe[iAD], yerr=sFe[iAD:iAD+lAD], fmt='o', color=Blue, lw=0.5,ms=2)
# ax3.plot([0,5], np.polyval(np.polyfit(Fe[iAD:iAD+lAD]-Fe[iAD], Fa[iAD:iAD+lAD]-Fa[iAD], 2), [0,5]), color=Blue)
# ax3.errorbar(Fa[iRD:iRD+lRD]-Fa[iRD],Fe[iRD:iRD+lRD]-Fe[iRD], yerr=sFe[iRD:iRD+lRD], fmt='o', color=Orange, lw=0.5,ms=2)
# ax3.plot([0,5], np.polyval(np.polyfit(Fa[iRD:iRD+lRD]-Fa[iRD], Fe[iRD:iRD+lRD]-Fe[iRD], 2), [0,5]), color=Orange)
# # set_plot(ax3, xlabel='$\\nu_a$ (Hz)', ylabel='$\delta \\nu_e$ (Hz)')

show()
