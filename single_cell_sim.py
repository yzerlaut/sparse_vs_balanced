import numpy as np
import os, sys
from model import Model
from graphs.my_graph import *

def my_logspace(x1, x2, n):
    return np.logspace(np.log(x1)/np.log(10), np.log(x2)/np.log(10), n)

Q_FACTOR = [0.1, 1., 5.] # chosen values to investigate

N = 40
FE = [my_logspace(4, 250, N),\
      my_logspace(0.9, 30, N),\
      my_logspace(0.02, 10, N)]
      
Desired_muV = [np.linspace(-68, -55, N)*1e-3,
               np.linspace(-65, -53, N)*1e-3,
               np.linspace(-68, -56, N)*1e-3]

N2 = 500
FI = my_logspace(1e-2,500,N2)

Model2 = Model.copy()
Model2['POP_STIM'] = ['RecExc', 'RecInh']
Model2['NRN_KEY'] = 'RecExc'
Model2['N_SEED'] = 4
Model2['tstop'] = 4000.
Model2['dt'] = 0.1

def evaluate_Vm_fluctuations(data,t_init=100, repolarization_blank=1, Trefrac=5):
    t = np.arange(len(data['Vm'][0,:]))*Model2['dt']
    muV, sV, gV, Tv = [], [], [], []
    for i in range(data['Vm'].shape[0]):
        tspikes = data['tspikes'][np.argwhere(data['ispikes']==i).flatten()]
        cond = (t>t_init)
        for tt in tspikes:
            cond = cond & np.invert((t>tt) & (t<tt+Trefrac+repolarization_blank))
        muV.append(np.mean(data['Vm'][i,:][cond]))
        sV.append(np.std(data['Vm'][i,:][cond]))
        gV.append(skew(data['Vm'][i,:][cond]))
        Tv.append(0)
    return np.array(muV), np.array(sV), np.array(gV), np.array(Tv)
        
if sys.argv[-1]=='run':
    
    from model import Model, pass_arguments_of_new_model
    from neural_network_dynamics.transfer_functions.single_cell_protocol import run_sim, my_logspace
    from neural_network_dynamics.cells.cell_library import built_up_neuron_params
    from neural_network_dynamics.theory.Vm_statistics import getting_statistical_properties
    from neural_network_dynamics.theory.tf import build_up_afferent_synaptic_input
    from scipy.stats import skew
    
    output = {'fout':np.zeros((N, len(Q_FACTOR))),
              'sfout':np.zeros((N, len(Q_FACTOR))),
              'FI':np.zeros((N, len(Q_FACTOR))),
              'FE':np.zeros((N, len(Q_FACTOR))),
              'SVth':np.zeros((N, len(Q_FACTOR))),
              'GV':np.zeros((N, len(Q_FACTOR))),
              'MUVsub':np.zeros((N, len(Q_FACTOR))),
              'sMUVsub':np.zeros((N, len(Q_FACTOR))),
              'SVsub':np.zeros((N, len(Q_FACTOR))),
              'sSVsub':np.zeros((N, len(Q_FACTOR))),
              'MUV':np.zeros((N, len(Q_FACTOR))),
              'sMUV':np.zeros((N, len(Q_FACTOR))),
              'SV':np.zeros((N, len(Q_FACTOR))),
              'sSV':np.zeros((N, len(Q_FACTOR)))}
    
    for iq, q in enumerate(Q_FACTOR):
        output['FE'][:, iq] = FE[iq]
        for e, fe in enumerate(FE[iq]):
            for key in ['Q_RecExc_RecExc', 'Q_RecExc_RecInh', 'Q_RecInh_RecExc', 'Q_RecInh_RecInh']:
                Model2[key] = Model[key]*q
            SYN_POPS = build_up_afferent_synaptic_input(Model2, ['RecExc', 'RecInh'], NRN_KEY='RecExc')
            muV, sV, gV, Tv = getting_statistical_properties(built_up_neuron_params(Model2, 'RecExc'),
                                                             SYN_POPS, {'F_RecExc':fe, 'F_RecInh':FI})
            i0 = np.argmin((muV-Desired_muV[iq][e])**2)
            Model2['RATES'] = {'F_RecExc':fe, 'F_RecInh':FI[i0]}
            output['SVth'][e, iq] = sV[i0]
            output['FI'][e, iq] = FI[i0]
            # WITH THRESHOLD
            Model2['RecExc_Vthre'] = -50 # with threshold
            # output['fout'][e, iq], output['sfout'][e, iq] = run_sim(Model2, firing_rate_only=True)
            data = run_sim(Model2, firing_rate_only=False, with_Vm=Model2['N_SEED'])
            output['fout'][e, iq], output['sfout'][e, iq] = data['fout_mean'], data['fout_std']
            # muV, sV, gV, Tv = evaluate_Vm_fluctuations(data)
            # output['MUV'][e, iq] = np.mean(muV)
            # output['sMUV'][e, iq] = np.std(muV)
            # output['SV'][e, iq] = np.mean(sV)
            # output['sSV'][e, iq] = np.std(sV)
            # # SUBTHRESHOLD
            # Model2['RecExc_Vthre'] = +500 # only subthreshold dynamics
            # data = run_sim(Model2, firing_rate_only=False, with_Vm=Model2['N_SEED'])
            # muV, sV, gV, Tv = evaluate_Vm_fluctuations(data)
            # output['MUVsub'][e, iq] = np.mean(muV)
            # output['sMUVsub'][e, iq] = np.std(muV)
            # output['SVsub'][e, iq] = np.mean(sV)
            # output['sSVsub'][e, iq] = np.std(sV)
            
    np.savez('sparse_vs_balanced/data/kuhn_et_al_data.npz', **output)
    
elif sys.argv[-1]=='plot':
    output = dict(np.load('sparse_vs_balanced/data/kuhn_et_al_data.npz'))
    fig, AX = figure(axes=(4,3))
    # AX = np.array(AX)
    # for a, ax in enumerate(AX[3]):
    #     plot(output['FE'][:,a], y=output['FI'][:,a], color=viridis(a/2.), ax=ax, lw=2)
    #     set_plot(ax, xscale='log', xlabel='$\\nu_e^{input}$ (Hz)', yscale='log', xlim=[FE[a][0],FE[a][-1]])
    # for a, ax in enumerate(AX[2]):
    #     ax.plot(output['FE'][:,a], 1e3*Desired_muV, 'k:', lw=3, label='desired $\mu_V$', alpha=.5)
    #     plot(output['FE'][:,a], y=output['MUVsub'][:,a], sy=output['sMUV'][:,a], color=viridis(a/2.), ax=ax, lw=2)
    #     set_plot(ax, xscale='log', xlim=[FE[a][0],FE[a][-1]])
    # AX[2][0].legend(frameon=False, prop={'size':'small'})
    # for a, ax in enumerate(AX[1]):
    #     ax.plot(output['FE'][:,a], 1e3*output['SVth'][:,a], 'k-', lw=3, label='analytical estimate \n(subthreshold dyn.)', alpha=.5)   
    #     plot(output['FE'][:,a], y=output['SVsub'][:,a], sy=output['sSV'][:,a], color=viridis(a/2.), ax=ax, lw=2)
    #     set_plot(ax, xscale='log', xlim=[FE[a][0],FE[a][-1]])
    # AX[1][1].legend(frameon=False, prop={'size':'small'})
    # for a, ax in enumerate(AX[0]):
    #     plot(output['FE'][:,a], y=output['fout'][:,a]+1e-2, sy=output['sfout'][:,a], color=viridis(a/2.), ax=ax, lw=2)
    #     set_plot(ax, xscale='log', yscale='log', xlim=[FE[a][0],FE[a][-1]])

    # AX[0][0].set_ylabel('$\\nu_e^{output}$ (Hz)')
    # AX[1][0].set_ylabel('$\\sigma_V$ (mV)')
    # AX[2][0].set_ylabel('$\\mu_V$ (mV)')
    # AX[3][0].set_ylabel('$\\nu_i^{input}$ (Hz)')

    fig2, AX2 = figure(axes=(1,2), wspace=.3, figsize=(.8,.9))
    ax1 = add_inset(AX2[0], [.75,.01,.25,.3])
    ax2 = add_inset(AX2[1], [.75,.01,.25,.3])
    
    Desired_muV[0][0] = -68
    
    ax1.plot(output['FE'][:,1], output['FI'][:,1], color=viridis(.5), lw=1)   
    ax2.plot(output['FE'][:,2], output['FI'][:,2], color=viridis(1.), lw=1)
    set_plot(ax1, ['top', 'left'], yscale='log', xscale='log', xlabel='$\\nu_e^{input}$', ylabel='$\\nu_i^{input}$',
             xticks=[1, 30], xticks_labels=['1', '30'],fontsize=FONTSIZE-1,
             yticks=[1, 30], yticks_labels=['1', '30'])
    set_plot(ax2, ['top', 'left'], yscale='log', xscale='log', xlabel='$\\nu_e^{input}$', ylabel='$\\nu_i^{input}$',
         xticks=[1e-1, 10], xticks_labels=['0.1  ', '  10'],fontsize=FONTSIZE-1,
         yticks=[1e-1, 10], yticks_labels=['0.1', '10'])
    AX2[0].plot(1e3*Desired_muV[1], output['fout'][:,1]+1e-2, color=viridis(.5), lw=3, alpha=.5)
    y, sy = output['fout'][:,1]+1e-2, output['sfout'][:,1]+1e-3
    sy[(y-sy)<1e-2] = y[(y-sy)<1e-2]-1e-2
    AX2[0].errorbar(1e3*Desired_muV[1], y, yerr=sy, color=viridis(.5), lw=1, ms=1, fmt='o')
     
    ax1.plot(output['FE'][:,1], output['FI'][:,1], color=viridis(.5), lw=1)   
    ax2.plot(output['FE'][:,2], output['FI'][:,2], color=viridis(1.), lw=1)   
    AX2[1].plot(1e3*Desired_muV[2], output['fout'][:,2]+1e-2, color=viridis(1.), lw=3, alpha=.5)
    y, sy = output['fout'][:,2]+1e-2, output['sfout'][:,2]+1e-3
    sy[(y-sy)<1e-2] = y[(y-sy)<1e-2]-1e-2
    AX2[1].errorbar(1e3*Desired_muV[2], y, yerr=sy, color=viridis(1.), lw=1, ms=1, fmt='o')

    AX2[0].annotate('$\\nu_e^{output}$ (Hz)', (-.6, .95), xycoords='axes fraction', rotation=90)
    set_plot(AX2[0], yscale='log', xlabel='$\mu_V$ (mV)', 
             xticks=[-65,-60,-55], ylim=[9.1e-3,69],
             yticks=[1e-2, 1e-1, 1, 10], yticks_labels=['<0.01', '0.1', '1', '10'], grid=False)
    set_plot(AX2[1], yscale='log', xlabel='$\mu_V$ (mV)', 
             xticks=[-65,-60,-55], ylim=[9.1e-3,69],
             yticks=[1e-2, 1e-1, 1, 10], yticks_labels=[], grid=False)

    fig2.suptitle('cellular properties of synaptic integration', fontsize=FONTSIZE)
    # fig.savefig(desktop+'fig1.svg')
    fig2.savefig(desktop+'fig2.svg')
    show()
else:
    # demo simulation
    fig, ax = figure(axes_extents=[[[2,1]]])
    Model2['N_SEED'] = 2
    # SYN_POPS = build_up_afferent_synaptic_input(Model, ['RecExc', 'RecInh'], NRN_KEY='RecExc')
    # muV, sV, gV, Tv = getting_statistical_properties(built_up_neuron_params(Model2, 'RecExc'),
    #                                                  SYN_POPS, {'F_RecExc':1., 'F_RecInh':np.linspace(1e-3,10,20)})
    # print(muV, sV, gV, Tv)
    data = run_sim(Model2, with_Vm=Model2['N_SEED'])
    print(data['Vm'].shape)
    # ax.plot(output['Vm'][0], 'k')
    # show()

